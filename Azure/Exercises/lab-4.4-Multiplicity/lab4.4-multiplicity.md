# Multiplicity

Lab Objective:
- Use for_each for multiple database firewall rules
- Use count for multiple VMs in an availability set

## Preparation

If you did not complete lab 4.3, you can simply copy the solution code from that lab (and do terraform apply) as the starting point for this lab.

Before working on this lab we will need a few more 'local' values related to the networks that we need to get from the `tf-base` project.   Switch to that project and add these additional "output" resources.

```
output "subnet-private" {
  value = azurerm_subnet.lab-private.id
}

output "subnet-public-address-prefix" {
  value = azurerm_subnet.lab-public.address_prefixes[0]
}
```

Now back on the `tf-project` project add two additional 'locals' with the values from the new created `tf-base` outputs:

- `subnet_private`
- `subnet_public_address_prefix`

_Note: Can you think of a better way to share data between Terraform projects?  Maybe improving the sharing of data between Terraform projects would make a good "extra" lab exercise._

## Lab

### Using `for_each`

Open `database.tf` file for edit.

Notice that we have two firewall rules that are essentially the same except for the name suffix and the ip addresses.  This would be a good case for using one of the multiplicity meta-arguments.  Can you articulate why for_each would be better to use in this case than using count?

The for_each operation requires a map.  What are the pieces of information that differ between the firewall rules?  Think about what the map might look like to capture the different values.

Add a locals block to the `database.tf` file and define a local map called `db_fw_rules` for the firewall rules. The map keys could be the name suffix ("private" and "bastion") and the values could be a sub-map for the start_ip and end_ip values.  Try your hand at writing the map, then check the solution below (or look in database.tf in the solution folder.)

<details>

 _<summary>Click to see solution for fire wall rule map</summary>_

This Terraform block defines a local variable using the `locals` block within a Terraform configuration. This specific block is creating a structured local variable named `db_fw_rules` that stores firewall rule settings for a database. Here's a breakdown of the configuration:

```
locals {
  db_fw_rules = {
    private = {
      start_ip = cidrhost(local.subnet_private_address_prefix,0)
      end_ip   = cidrhost(local.subnet_private_address_prefix,255)
    },
    bastion = {
      start_ip = azurerm_linux_virtual_machine.lab-bastion.private_ip_address
      end_ip   = azurerm_linux_virtual_machine.lab-bastion.private_ip_address
    }
  }  
}
```

### Local Variable: `db_fw_rules`
- **Type**: A map of maps, where each key represents a different set of firewall rule configurations for a database.

#### Nested Map Details:
1. **`private`**:
   - **Purpose**: Specifies firewall rule settings intended to allow traffic from a private subnet.
   - **Attributes**:
     - `start_ip`: The first IP in the subnet, calculated using the `cidrhost` function. This function calculates a specific host IP within a given CIDR block. Here, it's set to the first IP address (`0`) in the subnet defined by `local.subnet_private_address_prefix`.
     - `end_ip`: The last IP in the subnet, similarly calculated using the `cidrhost` function with the last possible host IP within the CIDR block (`255`).

2. **`bastion`**:
   - **Purpose**: Specifies firewall rule settings that allow traffic specifically from a bastion host.
   - **Attributes**:
     - `start_ip`: The private IP address of the bastion virtual machine, retrieved directly from the resource `azurerm_linux_virtual_machine` named `lab-bastion`.
     - `end_ip`: Identical to `start_ip` in this case, because the rule is meant to allow traffic only from this specific IP address, indicating a one-to-one IP rule.

</details>

 > Notice that we can have multiple "locals" blocks in Terraform projects as long as there is not a conflicting 'local' name.

### Usage Context:
- By using a `locals` block, the configuration abstracts and centralizes the definition of IP ranges and addresses, making it easier to manage and reference these firewall rules across potentially multiple resources without repeating complex expressions.

### Advantages of Using `locals` for Firewall Rules:
- **Maintainability**: Centralizing IP configuration in `locals` makes it simpler to update IP addresses or ranges in one place rather than throughout various parts of the Terraform code.
- **Readability**: Using named maps like `private` and `bastion` makes the Terraform code more readable and understandable, especially for teams managing the infrastructure.
- **Reusability**: Defined once, these local values can be reused in multiple resource configurations, ensuring consistency and reducing the potential for errors.

Replace the two firewall rule resources by a single resource that uses the `for_each`.  Notice that we can use the map key as well as the map values.

```
resource "azurerm_postgresql_firewall_rule" "lab" {
  for_each = local.db_fw_rules

  name                = "aztf-labs-fwrule-${each.key}"
  resource_group_name = local.resource_group
  server_name         = azurerm_postgresql_server.lab.name
  start_ip_address    = each.value.start_ip
  end_ip_address      = each.value.end_ip
}
```

Run terraform validate:

```shell
terraform validate
```

Run `terraform plan`.  You should get a plan that will destroy the original firewall resources and create replacement firewall rule resources.

:question: Can you identify why this is the case even though the Azure name for the two rules has not changed?  (*Hint: What other name change could cause a resource to be re-created?*)

```shell
terraform plan
```

![Terraform Plan - for_each](./images/tf-plan-foreach.png "Terraform Plan - for_each")

Run `terraform apply`.

> :bangbang: Note, you might get an error that the replacement resources could not be created because the resource still exists.  This is due to a timing conflict between the destroy and create processing when the Azure name is the same between the old and new instances.  The destroy should have succeeded, however, and you can simply run terraform apply a second time to perform the create.

```shell
terraform apply
```

---

### Using count

To show the use of count, we will create an availability set of virtual machines behind a load balancer.

Create a new file called `lb.tf` put the following Terraform configurations in the new file to  declare five new resources:

* A public IP for the load balancer
* A load balancer
* A backend address pool for the virtual machines behind the load balancer
* A health probe for the load balancer to detect whether or not a virtual machine is healthy
* A routing rule for directing incoming traffic to the virtual machines

```
resource "azurerm_public_ip" "lab-lb" {
  name                         = "aztf-labs-lb-public-ip"
  location                     = local.region
  resource_group_name          = local.resource_group
  allocation_method            = "Static"
  tags                         = local.common_tags
}

resource "azurerm_lb" "lab" {
  name                = "aztf-labs-loadBalancer"
  location            = local.region
  resource_group_name = local.resource_group

  frontend_ip_configuration {
    name                 = "publicIPAddress"
    public_ip_address_id = azurerm_public_ip.lab-lb.id
  }

  tags = local.common_tags
}

resource "azurerm_lb_backend_address_pool" "lab" {
  loadbalancer_id     = azurerm_lb.lab.id
  name                = "BackEndAddressPool"
}

resource "azurerm_lb_probe" "lab" {
  loadbalancer_id     = azurerm_lb.lab.id
  name                = "http-running-probe"
  protocol            = "Http"
  port                = 80
  request_path        = "/"
}

resource "azurerm_lb_rule" "lab" {
  loadbalancer_id                = azurerm_lb.lab.id
  name                           = "aztf-labs-lb-rule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "publicIPAddress"
  backend_address_pool_ids       = [azurerm_lb_backend_address_pool.lab.id]
  probe_id                       = azurerm_lb_probe.lab.id
}
```

### LB block Description

#### 1. `azurerm_public_ip` Resource
- **Resource Type**: `azurerm_public_ip`
- **Resource Name**: `lab-lb`
- **Purpose**: Creates a static public IP address in Azure, which will be used by the load balancer.
- **Attributes**:
  - `name`: Specifies the name of the public IP resource.
  - `location`: Sets the geographic location for the resource based on a local variable.
  - `resource_group_name`: Specifies the resource group under which this public IP will be created, using a local variable.
  - `allocation_method`: Defines the allocation method as "Static", meaning the IP address will not change.
  - `tags`: A map of metadata tags associated with the resource, using locally defined common tags.

#### 2. `azurerm_lb` Resource
- **Resource Type**: `azurerm_lb`
- **Resource Name**: `lab`
- **Purpose**: Configures an Azure load balancer.
- **Attributes**:
  - `name`: The name of the load balancer.
  - `location`: The location, set to match that of the public IP and other related resources.
  - `resource_group_name`: Specifies the resource group.
  - `frontend_ip_configuration`: Configures the frontend IP settings for the load balancer, associating it with the previously created static public IP.
    - `name`: A name for the frontend IP configuration.
    - `public_ip_address_id`: Links to the `azurerm_public_ip` resource by its ID.

#### 3. `azurerm_lb_backend_address_pool` Resource
- **Resource Type**: `azurerm_lb_backend_address_pool`
- **Resource Name**: `lab`
- **Purpose**: Creates a backend address pool for the Azure load balancer, where backend servers will be added to handle the load-balanced traffic.
- **Attributes**:
  - `loadbalancer_id`: The ID of the load balancer with which this backend address pool is associated.
  - `name`: Specifies the name of the backend address pool.

#### 4. `azurerm_lb_probe` Resource
- **Resource Type**: `azurerm_lb_probe`
- **Resource Name**: `lab`
- **Purpose**: Sets up a health probe to monitor the health of the servers in the backend address pool using HTTP requests.
- **Attributes**:
  - `loadbalancer_id`: Associates the probe with the specific load balancer.
  - `name`: The name of the probe.
  - `protocol`: The protocol used for the health check, set to "Http".
  - `port`: The port on which the health check will be performed, here using port 80.
  - `request_path`: The path used for the health check, set to the root path ("/").

#### 5. `azurerm_lb_rule` Resource
- **Resource Type**: `azurerm_lb_rule`
- **Resource Name**: `lab`
- **Purpose**: Defines a rule for the load balancer that specifies how traffic should be handled.
- **Attributes**:
  - `loadbalancer_id`: Links the rule to the specific load balancer.
  - `name`: Names the load balancing rule.
  - `protocol`: The protocol for the traffic, set to "Tcp".
  - `frontend_port`: The port on which the load balancer listens, set to 80.
  - `backend_port`: The port to which traffic is forwarded in the backend, also set to 80.
  - `frontend_ip_configuration_name`: Specifies which frontend IP configuration to use.
  - `backend_address_pool_ids`: An array that includes the ID of the backend address pool.
  - `probe_id`: Links to the health probe that is used to check the availability of the backend resources.

These resources together configure a fully functional Azure load balancer with a static public IP, a backend address pool, a health probe to monitor backend health via HTTP, and a rule defining traffic management. This setup is for applications requiring high availability, fault tolerance, or distribution of traffic across multiple servers.

### Outputs

Open `outputs.tf` and add a new output so we can easily get the load balancer public IP:

```
output "load-balancer-public-ip" {
  value = azurerm_public_ip.lab-lb.ip_address
}
```

### VM Cluster

Open `local.tf` and add a new local value for `cluster_size`.  Set it to a value of `2`.

Create a new file called `vm-cluster.tf`

We will create four resources in this file.  Notice that we will be using the count meta-argument in three of the resources to create multiple instances of the resources.  The count will be based on a new local (which we will add later) to set the size of the VM cluster.

1.  Add a network interface for the virtual machines to have a private IP on the private subnet.  We use count to create a separate NIC for each virtual machine.

```
resource "azurerm_network_interface" "lab-app" {
  count               = local.cluster_size
  name                = "aztf-labs-nic-${count.index}"
  location            = local.region
  resource_group_name = local.resource_group

  ip_configuration {
    name                          = "labConfiguration"
    subnet_id                     = local.subnet_private
    private_ip_address_allocation = "Dynamic"
  }

  tags = local.common_tags
}
```

#### `azurerm_network_interface` Resource
- **Resource Type**: `azurerm_network_interface`
- **Name**: `lab-app`
- **Purpose**: Creates a network interface for each virtual machine in the cluster.
- **Attributes**:
  - `count`: Determines the number of network interfaces to create based on the `local.cluster_size` variable.
  - `name`: Names each network interface uniquely using `count.index` to differentiate between them.
  - `location` and `resource_group_name`: Specifies the Azure region and resource group for the network interfaces, derived from local variables.
  - `ip_configuration`: Configures IP settings for each interface, including:
    - `name`: A common name for all IP configurations.
    - `subnet_id`: The subnet ID where the interfaces will reside, pulled from a local variable.
    - `private_ip_address_allocation`: Set to "Dynamic", allowing Azure to assign IP addresses dynamically.
  - `tags`: A set of metadata tags applied to each network interface for easier management and categorization.



2. Add an association between the virtual machines and the load balancer.  We use count to create a separate association for each virtual machine.

```
resource "azurerm_network_interface_backend_address_pool_association" "lab-app" {
  count                   = local.cluster_size
  network_interface_id    = azurerm_network_interface.lab-app[count.index].id
  ip_configuration_name   = "labConfiguration"
  backend_address_pool_id = azurerm_lb_backend_address_pool.lab.id
}
```

#### `azurerm_network_interface_backend_address_pool_association` Resource
- **Resource Type**: `azurerm_network_interface_backend_address_pool_association`
- **Name**: `lab-app`
- **Purpose**: Associates each network interface with a backend address pool of a load balancer, enabling the VMs to participate in load balancing.
- **Attributes**:
  - `count`: Matches the count of network interfaces, ensuring each interface is associated.
  - `network_interface_id`: References the ID of each network interface created.
  - `ip_configuration_name`: Specifies the IP configuration name within the network interface to be associated.
  - `backend_address_pool_id`: The ID of the load balancer's backend address pool where these interfaces will be connected.

3. Add an availability set.  We do not use count here since we only need one availability set for the entire cluster.

```
resource "azurerm_availability_set" "lab-app" {
  name                         = "aztf-labs-avset"
  location                     = local.region
  resource_group_name          = local.resource_group
  tags                         = local.common_tags
}
```

#### `azurerm_availability_set` Resource
- **Resource Type**: `azurerm_availability_set`
- **Name**: `lab-app`
- **Purpose**: Creates an availability set to improve the fault tolerance of the deployed VMs.
- **Attributes**:
  - `name`: Names the availability set.
  - `location` and `resource_group_name`: Defines the placement and organizational context of the availability set.
  - `tags`: Applies metadata tags for management and categorization.

4. Add the virtual machines.  We use count here to create multiple virtual machines.  Note that the VMs are identical other than references to count.index.

```
resource "azurerm_linux_virtual_machine" "lab-app" {
  count                 = local.cluster_size
  name                  = "aztf-labs-app-vm-${count.index}"
  resource_group_name   = local.resource_group
  location              = local.region
  size                  = "Standard_B1s"
  network_interface_ids = [azurerm_network_interface.lab-app[count.index].id]
  availability_set_id   = azurerm_availability_set.lab-app.id
  admin_username        = "adminuser"
  admin_password        = var.vm_password
  disable_password_authentication = false

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-confidential-vm-jammy"
    sku       = "22_04-lts-cvm"
    version   = "22.04.202210040"
  }

  tags = local.common_tags
}
```

#### `azurerm_linux_virtual_machine` Resource
- **Resource Type**: `azurerm_linux_virtual_machine`
- **Name**: `lab-app`
- **Purpose**: Deploys a cluster of Linux virtual machines configured for high availability and load balancing.
- **Attributes**:
  - `count`: The number of VMs to create, driven by `local.cluster_size`.
  - `name`: Each VM is uniquely named using `count.index`.
  - `resource_group_name`, `location`, and `size`: Configures the basic settings of the VMs.
  - `network_interface_ids`: Connects each VM to its respective network interface.
  - `availability_set_id`: Assigns VMs to the previously created availability set.
  - `admin_username` and `admin_password`: Sets up the administrative credentials for the VMs. Note the use of a variable for the password, indicating sensitivity.
  - `disable_password_authentication`: Indicates password login is enabled, though it's less secure than SSH keys.
  - `os_disk`: Configures the operating system disk properties.
  - `source_image_reference`: Specifies the VM image to use, here an Ubuntu server image.
  - `tags`: Applies the same set of common metadata tags.


This Terraform configuration is designed for a highly available application infrastructure, with each VM in the cluster configured to handle load-balanced traffic within a controlled fault-tolerant environment. This setup is ideal for scenarios where high availability and load distribution are critical, such as in production web application environments.

---

#### Security Group Rules

We need to create a new network security group for the VM Cluster on the lab-private subnet.  This will need to be done over in the `tf-base` project because to be able to associate the security group with the subnet_id managed in that project.  Create and open for edit a new `tf-base/vm-cluster-security.tf` file. 

Add a new security group for the private subnet that will allow traffic to ports 80 and 22 on our new virtual machines.  Add it below the current security group.

```
resource "azurerm_network_security_group" "lab-private" {
  name                = "aztf-labs-private-sg"
  location            = "eastus2"
  resource_group_name = azurerm_resource_group.base.name

  security_rule {
    name                       = "HTTP-Access"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "*"
    destination_address_prefixes = azurerm_subnet.lab-private.address_prefixes
  }

  security_rule {
    name                       = "SSH-Access"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefixes = azurerm_subnet.lab-private.address_prefixes
  }
}
```

Associate the new security group to the private subnet.  Add it below the current security group association resource.

```
resource "azurerm_subnet_network_security_group_association" "lab-private" {
  subnet_id                 = azurerm_subnet.lab-private.id
  network_security_group_id = azurerm_network_security_group.lab-private.id
}
```

Now run `terraform apply` on the `tf-base` project to add the security group needed for the VM Cluster.  Once that is applied switch back to the `tf-project` folder.

#### Deploy Cluster

Okay, that's a lot of edits.  Be sure to run terraform validate to make sure you got everything.

```shell
terraform validate
```

Run `terraform plan` to verify what will be created.  Scroll through the plan.  Does it match what you would expect?  Notice how the plan uses an array index to reference the multiple instances of a resource.

```shell
terraform plan
```

![Terraform Plan - Count](./images/tf-plan-count.png "Terraform Plan - count")

Run `terraform apply`:

```shell
terraform apply
```

### (Optional) Trying out the load balanced cluster

If you have extra time now or later, you can verify that the load balancer actually works to connect to the clustered VMs.  See the instructions at [Testing Your Cluster](../optional-material/testing_your_cluster.md).

---

### Summary

#### Lab Objectives:
- Implement and understand the use of `for_each` for creating multiple database firewall rules.
- Utilize `count` to manage multiple virtual machines within an availability set.

#### Preparation:
Participants should start with the infrastructure set up from lab 4.3. This includes outputs for subnet IDs that will be used to configure network settings in the current lab.

#### Lab Steps:

### Step 1: Update Base Configuration
- **Outputs Addition in tf-base Project**: Participants are instructed to add outputs for private subnet ID and public subnet address prefix. This facilitates the sharing of essential networking information between projects.
- **Local Values Setup in tf-project**: Two new local variables, `subnet_private` and `subnet_public_address_prefix`, are created to store values from the tf-base project outputs.

### Step 2: Implement `for_each` for Firewall Rules
- **Database Firewall Configuration**:
  - Transition from individual resource blocks for firewall rules to a single block using `for_each`.
  - A `locals` block defines a map `db_fw_rules` containing configurations for each rule, facilitating the dynamic creation of firewall rules based on the map's entries.
- **Firewall Rule Resource**:
  - A new `azurerm_postgresql_firewall_rule` resource uses `for_each` to iterate over each entry in the `db_fw_rules` map, applying configurations such as start and end IP addresses.

### Step 3: Implement `count` for VM Clusters
- **Load Balancer Setup**:
  - Participants create a new file `lb.tf` and configure a load balancer, including a public IP, backend address pool, health probe, and load balancing rule.
- **VM Cluster Configuration**:
  - A new file `vm-cluster.tf` is created to manage the deployment of a cluster of virtual machines.
  - The `azurerm_network_interface` and `azurerm_network_interface_backend_address_pool_association` resources use `count` to create multiple instances based on the cluster size defined in local variables.
  - An `azurerm_linux_virtual_machine` resource also uses `count` to deploy multiple VMs, linking each with its corresponding network interface and configuring them within an availability set.

### Step 4: Security Group Rules in tf-base
- **Security Group Addition**:
  - A new network security group with rules allowing HTTP and SSH access is configured in the `tf-base` project to ensure the subnet's security settings support the VM cluster.

### Step 5: Validation and Application
- Participants run `terraform validate` to ensure there are no configuration errors.
- `terraform plan` is executed to review the changes, which should show the replacement of firewall resources and the creation of new VMs and load balancer components.
- `terraform apply` is used to apply the changes, with a potential need to run it twice if timing conflicts occur between resource destruction and creation.

### Educational Outcomes:
- **Understanding Multiplicity**: Participants gain a deep understanding of how to use `for_each` and `count` to manage multiple resources dynamically, which is crucial for scaling infrastructure efficiently.
- **Inter-Project Data Sharing**: The lab demonstrates a method to share data between Terraform projects using outputs and locals, which is essential for managing large-scale Terraform implementations across multiple projects.
- **Practical Load Balancer Configuration**: Setting up a load balancer with backend pools, probes, and rules provides practical experience in managing traffic distribution for high-availability applications.

#### Conclusion:
This lab provides hands-on experience with advanced Terraform features such as `for_each` and `count`, teaching participants how to efficiently manage multiple instances of resources. By integrating these concepts with real-world tasks like setting up a load-balanced VM cluster, participants enhance their ability to design and implement scalable and resilient infrastructure solutions.
