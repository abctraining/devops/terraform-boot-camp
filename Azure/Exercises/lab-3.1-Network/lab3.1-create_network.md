# Create a Network

Lab Objective:
- Create a simple network in Azure

## Lab

In this lab we will be adding Terraform blocks for a virtual network in Azure, along with subnets and network security settings. This Terraform configuration establishes a network architecture consisting of a virtual network, two subnets (one public and one private), and a security group for the public subnet. This setup is typical for scenarios where you need to segregate resources that are exposed to the public internet from those that are not.


Open the file 'main.tf' file in the `tf-base` project folder to edit. Make the following changes to the file:

1. Add a resource for a virtual network.  

```
resource "azurerm_virtual_network" "lab" {
  name                = "aztf-labs-vnet"
  location            = "eastus2"
  resource_group_name = azurerm_resource_group.base.name
  address_space       = ["10.0.0.0/16"]
  tags                = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}
```

### Virtual Network Resource
- **Resource Type**: `azurerm_virtual_network`
- **Name**: `lab`
- **Properties**:
  - `name`: The name of the virtual network, set as "aztf-labs-vnet".
  - `location`: Geographic location of the virtual network, specified as "eastus2".
  - `resource_group_name`: References the name of the resource group created by another resource, `azurerm_resource_group.base.name`.
  - `address_space`: Defines the CIDR blocks that the virtual network will use. Here, it's set to ["10.0.0.0/16"].
  - `tags`: Key-value pairs providing metadata ("Environment: Lab", "Project: AZTF Training") to help categorize and organize Azure resources.

2. Add a resource for two subnets (public and private subnets).  Notice that the subnet CIDR blocks are within the VNet CIDR range.

```
resource "azurerm_subnet" "lab-public" {
  name                 = "aztf-labs-subnet-public"
  resource_group_name  = azurerm_resource_group.base.name
  virtual_network_name = azurerm_virtual_network.lab.name
  address_prefixes     = ["10.0.0.0/24"]
}

resource "azurerm_subnet" "lab-private" {
  name                 = "aztf-labs-subnet-private"
  resource_group_name  = azurerm_resource_group.base.name
  virtual_network_name = azurerm_virtual_network.lab.name
  address_prefixes     = ["10.0.1.0/24"]
}
```

### Subnet Resources
Two subnets are defined within the virtual network:
1. **Public Subnet**:
   - **Resource Type**: `azurerm_subnet`
   - **Name**: `lab-public`
   - **Properties**:
     - `name`: "aztf-labs-subnet-public"
     - `resource_group_name`: Inherited from the parent resource group.
     - `virtual_network_name`: References the virtual network name.
     - `address_prefixes`: CIDR block "10.0.0.0/24" designated for this subnet.

2. **Private Subnet**:
   - **Resource Type**: `azurerm_subnet`
   - **Name**: `lab-private`
   - **Properties**:
     - `name`: "aztf-labs-subnet-private"
     - `resource_group_name`: Again linked to the same resource group.
     - `virtual_network_name`: Linked to the same virtual network as the public subnet.
     - `address_prefixes`: CIDR block "10.0.1.0/24" designated for this subnet.


3. Add a resource for a security group.  For now we do not include a security group rule.

```
resource "azurerm_network_security_group" "lab-public" {
  name                = "aztf-labs-public-sg"
  location            = "eastus2"
  resource_group_name = azurerm_resource_group.base.name
}
```

### Network Security Group Resource
- **Resource Type**: `azurerm_network_security_group`
- **Name**: `lab-public`
- **Properties**:
  - `name`: "aztf-labs-public-sg"
  - `location`: "eastus2", consistent with other resources.
  - `resource_group_name`: Linked to the resource group of the virtual network and subnets.

4. Add a resource to associate the security group to the public subnet.

```
resource "azurerm_subnet_network_security_group_association" "lab-public" {
  subnet_id                 = azurerm_subnet.lab-public.id
  network_security_group_id = azurerm_network_security_group.lab-public.id
}
```

### Subnet and Network Security Group Association
- **Resource Type**: `azurerm_subnet_network_security_group_association`
- **Name**: `lab-public`
- **Properties**:
  - `subnet_id`: Links this association to the ID of the public subnet (`azurerm_subnet.lab-public.id`).
  - `network_security_group_id`: Links the subnet to the ID of the network security group (`azurerm_network_security_group.lab-public.id`).

5. Add an output to share the public subnets id.

```
output "subnet-public" {
  value = azurerm_subnet.lab-public.id
}
```

### Output
- **Name**: `subnet-public`
- **Purpose**: Outputs the ID of the public subnet (`azurerm_subnet.lab-public.id`), which can be useful for referencing this subnet in other configurations or outputs, such as when configuring services that require a subnet ID.

---

Run terraform validate.

```shell
terraform validate
```

Run terraform plan.

```shell
terraform plan
```

Run terraform apply to create all the new infrastructure.

```shell
terraform apply
```

Again take note of the output.  We will need the 'subnet-public' ID in the next lab.

### Viewing Results in the Azure Portal

Let's use the Azure Portal to see what we just created.  Minimize the Cloud Shell console so you can see the Azure Portal UI fully.

In the search bar at the top of the Azure Portal page, type in “resource”.  Select “Resource Groups” from the auto suggest drop-down.

You should see the following. (If you do not see the aztf-labs-rg resource group, click the Refresh icon above the resource list.)

![Azure Resource Groups](./images/az-rg.png "Azure Resource Groups")
<br /><br />

Click on the "aztf-labs-rg" resource group created by Terraform. (The other resource groups were created by other means in support of this class. You can ignore them.)

Confirm you see the virtual network and security group listed.<br />

![Resource Group containing virtual network and security group](./images/az-rg-vnet.png "Resource Group containing virtual network and security group")

<br /><br />
Click on the virtual network and confirm it has the expected subnets, and that the public subnet has the expected security group.

![Virtual network subnets and security group](./images/az-vnet-subnets.png "Virtual network subnets and security group")

---

### Summary

#### Lab Objective
The lab aims to guide participants through the process of creating a network in Azure using Terraform. This includes setting up a virtual network, subnets (both public and private), and a network security group.

#### Lab Setup
Participants will work within the `tf-base` project folder, editing the `main.tf` file to define the necessary Terraform resources.

#### Resource Definitions
1. **Virtual Network**:
   - Defined using the `azurerm_virtual_network` resource type.
   - Configured with a name, location, the encompassing resource group, a CIDR block for the address space, and descriptive tags.

2. **Subnets**:
   - Two subnets are created within the virtual network:
     - **Public Subnet**: Designated for publicly accessible resources, specified with its own CIDR block.
     - **Private Subnet**: Used for internally accessed resources, also with a specific CIDR block.

3. **Network Security Group**:
   - Defined using the `azurerm_network_security_group` resource type for the public subnet to manage access and security rules.

4. **Subnet-Network Security Group Association**:
   - Associates the public subnet with the created network security group to enforce security rules for traffic coming in and out of the subnet.

5. **Outputs**:
   - The configuration includes an output for the public subnet's ID, which can be useful for reference in other configurations or outputs.

#### Commands to Execute
Participants will run the following Terraform commands to validate, plan, and apply their configurations:
- `terraform validate`: Checks the syntax and validity of the Terraform files.
- `terraform plan`: Displays the changes that will be made.
- `terraform apply`: Executes the plan to create the infrastructure on Azure.

#### Azure Portal Interaction
After applying the Terraform configuration, participants are instructed to use the Azure Portal to visually inspect the resources they've created. This helps confirm the successful creation of the virtual network, subnets, and network security group, and ensures that configurations are as expected.

This lab provides hands-on experience with basic networking components in Azure using Terraform, emphasizing real-world applications like segmenting network resources and enhancing security with network security groups.