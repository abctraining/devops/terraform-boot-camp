# Postgresql Database

Lab Objective:
- Add a Postgresql server and database to your infrastructure

## Preparation

If you did not complete lab 3.3, you can simply copy the solution code from that lab (and run terraform apply) as the starting point for this lab.

## Lab

Create a new file `database.tf` in the `tf-project` folder

```shell
touch database.tf
```

Open the file for edit to add five new resources to define several resources related to setting up and securing a PostgreSQL database server on Azure using Terraform. Each resource plays a specific role in the architecture of the database deployment.

1. A random number.  This will be needed as a suffix for the postgresql server name to make that server name unique in Azure.

__You will need to add the `random` provider to your main terraform block and run `terraform init` to be able to use this `random_integer` resource.__ We did use this same provider in lab-2.2. 

```
resource "random_integer" "suffix" {
  min = 10000
  max = 99999
}
```

### `random_integer` Resource
- **Resource Type**: `random_integer`
- **Name**: `suffix`
- **Purpose**: Generates a random integer between 10000 and 99999. This is used to ensure the uniqueness of resource names where required, in this case, to help create a uniquely named PostgreSQL server.

2. A PostgreSQL server:

```
resource "azurerm_postgresql_server" "lab" {
  name                = "aztf-labs-psqlserver-${random_integer.suffix.result}"
  location            = local.region
  resource_group_name = local.resource_group

  sku_name                         = "B_Gen5_1"
  version                          = "11"
  storage_mb                       = 5120
  public_network_access_enabled    = true
  ssl_enforcement_enabled          = false
  ssl_minimal_tls_version_enforced = "TLSEnforcementDisabled"

  administrator_login           = "psqladmin"
  administrator_login_password  = "AZtfl4b$"

  tags = local.common_tags
}
```

### `azurerm_postgresql_server` Resource
- **Resource Type**: `azurerm_postgresql_server`
- **Name**: `lab`
- **Configuration**:
  - `name`: Combines a base name with the result from the `random_integer` resource to form a unique server name.
  - `location`: Specifies the geographic location for the server, using a locally defined variable `local.region`.
  - `resource_group_name`: Uses the name of an existing resource group defined in Terraform.
  - `sku_name`: Specifies the SKU name for the PostgreSQL server, indicating a basic, general-purpose tier with 1 vCore (`B_Gen5_1`).
  - `version`: The version of PostgreSQL deployed, specified here as version 11.
  - `storage_mb`: Configures the amount of storage for the database server, set to 5120 MB.
  - `public_network_access_enabled`: Allows the server to be accessible over the public internet.
  - `ssl_enforcement_enabled`: Indicates whether SSL connections are enforced for security. Here, it is set to `false`, which means SSL is not enforced but is allowed.
  - `ssl_minimal_tls_version_enforced`: Specifies the minimal version of TLS that is enforced for connections to this server. It's set to "TLSEnforcementDisabled", which means that no specific TLS version is enforced, potentially allowing any version of TLS to be used.

  - `administrator_login` and `administrator_login_password`: Credentials for the database administrator.
  - `tags`: A map of metadata tags applied to the resource for organization and management, using locally defined common tags.

3. A database in the server:

```
resource "azurerm_postgresql_database" "lab" {
  name                = "aztf-labs-db"
  resource_group_name = local.resource_group
  server_name         = azurerm_postgresql_server.lab.name
  charset             = "UTF8"
  collation           = "English_United States.1252"
}
```

### `azurerm_postgresql_database` Resource
- **Resource Type**: `azurerm_postgresql_database`
- **Name**: `lab`
- **Configuration**:
  - `name`: Names the database "aztf-labs-db".
  - `resource_group_name`: Specifies the resource group where the database is hosted.
  - `server_name`: Links the database to the previously created PostgreSQL server.
  - `charset`: Sets the character set for the database, here as "UTF8".
  - `collation`: Sets the collation for the database, specified as "English_United States.1252".

4. A firewall rule to enable database access from the private subnet.

Before adding this resource we first need to get a bit more information from the `tf-base` project.  We will need the Private Subnet CIDR Address.  Add the following output and "apply" it to the `tf_base/main.tf` file.

```
output "subnet-private-address-prefix" {
  value = azurerm_subnet.lab-private.address_prefixes[0]
}
```

After the "apply" you should now see the Private Subnet CIDR Address.  Back in the `tf-project` folder add a new "local" called `subnet_private_address_prefix` set to that CIDR value in `local.tf`.  

Now we can reference this new local back in the `tf-project/database.tf` file, there it is using the 'cidrhost()' filter.   We will cover "filters" in more detail later.

```
resource "azurerm_postgresql_firewall_rule" "lab-rule1" {
  name                = "aztf-labs-fwrule-private"
  resource_group_name = local.resource_group
  server_name         = azurerm_postgresql_server.lab.name
  start_ip_address    = cidrhost(local.subnet_private_address_prefix,0)
  end_ip_address      = cidrhost(local.subnet_private_address_prefix,255)
}
```
  
5. A firewall rule to enable database access from the bastion host.

```
resource "azurerm_postgresql_firewall_rule" "lab-rule2" {
  name                = "aztf-labs-fwrule-bastion"
  resource_group_name = local.resource_group
  server_name         = azurerm_postgresql_server.lab.name
  start_ip_address    = azurerm_linux_virtual_machine.lab-bastion.private_ip_address
  end_ip_address      = azurerm_linux_virtual_machine.lab-bastion.private_ip_address
}
```

### `azurerm_postgresql_firewall_rule` Resources
Two firewall rules are defined to control access to the PostgreSQL server:
- **`lab-rule1`**:
  - `name`: "aztf-labs-fwrule-private".
  - `resource_group_name`: The resource group containing the database server.
  - `server_name`: The name of the PostgreSQL server.
  - `start_ip_address` and `end_ip_address`: Defines a range of IP addresses allowed to access the database, calculated using the `cidrhost` function applied to the address range of a private subnet, allowing any IP within that subnet.

- **`lab-rule2`**:
  - `name`: "aztf-labs-fwrule-bastion".
  - `resource_group_name`: The resource group containing the database server.
  - `server_name`: The name of the PostgreSQL server.
  - `start_ip_address` and `end_ip_address`: Set to the private IP address of a specific Linux VM, presumably a bastion host. This rule allows only this specific VM to access the database.

These Terraform configurations are crafted to establish a secure, managed PostgreSQL server environment in Azure. The setup includes not only the database server itself but also security through firewall rules that restrict access to the server both from a specific VM and a private subnet. This configuration is typical for environments where security and controlled access to database resources are critical.

Look through the resources for a moment. What is the processing order dependency between the resources?

Run terraform validate to make sure you have no errors:

```shell
terraform validate
```

Run terraform plan and verify that only five new resources will be created.

```shell
terraform plan
```

![Terraform Plan - DB](./images/tf-plan-db.png "Terraform Plan - DB")


Run terraform apply. (Remember to agree to the changes.)  The database server can sometimes take a few minutes to create.

```shell
terraform apply
```

![Terraform apply - database create](./images/tf-apply-db.png "Terraform apply - database create")

### View Results in Azure Portal

Go to the Azure Portal.  Type “postgres” and select the “Azure Database for PostgreSQL servers” auto-suggestion.  (Do not pick “Azure Database for PostgreSQL servers v2”)

![Azure portal - Search for Postgres](./images/az-postgres.png "Azure portal - Search for Postgres")
<br /><br />

Click on the database server name in the list.

![Azure portal - db server](./images/az-dbserver.png "Azure portal - db server")
<br /><br />

Click on Connection Security under Settings in the left navigation pane to confirm the firewall rules are present.

![Azure portal - db firewall](./images/az-dbfw.png "Azure portal - db firewall")

---

### Summary

#### Lab Objective:
The primary goal of this lab is to integrate a PostgreSQL server and database into your Azure infrastructure, emphasizing the use of Terraform for resource management and security configuration.

#### Preparation:
Participants should start from the setup provided in lab 3.3 or copy its solution code to ensure they have the necessary infrastructure components in place.

#### Lab Setup:
1. **File Creation**: Participants are instructed to create a new file named `database.tf` in the `tf-project` folder. This file will centralize all configurations related to the PostgreSQL database.

#### Steps and Configurations:
1. **Random Integer Generation**:
   - A `random_integer` resource named `suffix` is created to generate a unique identifier for the PostgreSQL server, ensuring that the server name is unique within Azure.

2. **PostgreSQL Server Configuration**:
   - A resource of type `azurerm_postgresql_server` named `lab` is configured with dynamic inputs for uniqueness and local variables for region and resource group settings.
   - The server setup includes basic performance specifications, version control, and administrative credentials.
   - Public network access is enabled while SSL enforcement is disabled, tailored to a development or controlled environment.

3. **Database Creation**:
   - An `azurerm_postgresql_database` resource named `lab` is defined to reside within the newly created server, using standard UTF-8 character encoding and a specific collation for English (United States).

4. **Firewall Rules**:
   - Two `azurerm_postgresql_firewall_rule` resources (`lab-rule1` and `lab-rule2`) are established to manage access to the PostgreSQL server.
     - **`lab-rule1`** allows access from a private subnet.
     - **`lab-rule2`** restricts access to just the bastion host, enhancing security by limiting database access to a controlled point.

#### Execution Instructions:
- **`terraform validate`**: Ensures the configuration files are free of syntax errors.
- **`terraform plan`**: Provides a preview of the resources Terraform will create, modify, or destroy.
- **`terraform apply`**: Applies the defined Terraform configurations, effectively setting up the PostgreSQL database server, the database itself, and associated firewall rules.

#### Viewing Results:
Participants are guided to verify the successful creation and configuration of the PostgreSQL resources through the Azure Portal, particularly checking the connection security settings to confirm that the firewall rules are correctly applied.

#### Educational Goals:
- **Hands-on Experience**: Participants gain practical experience in managing database solutions in the cloud using Terraform, from server creation to detailed security settings.
- **Security Emphasis**: The lab focuses on security best practices by demonstrating how to restrict database access via firewall rules.
- **Resource Management**: The lab highlights the importance of using Terraform for resource management to ensure configurations are reproducible and manageable.

#### Conclusion:
This lab effectively demonstrates how to extend an Azure infrastructure to include a fully managed PostgreSQL database using Terraform. It showcases the integration of security practices within infrastructure as code, preparing participants for more advanced cloud management tasks.
