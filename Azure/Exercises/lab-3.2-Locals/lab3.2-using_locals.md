# Using Locals

Lab Objective:
- Use locals to set values used more than once

## Preparation

If you did not complete lab 3.1, you can simply copy the solution code from that lab (and run terraform apply) as the starting point for this lab.

In previous labs we have created a foundation for Azure Resources to be managed within terraform projects.

From this point forward we will be using the third and main project folder in which we will create the resources in Azure.  Create a project folder for this lab `~/clouddrive/Projects/tf-project`.

```shell
mkdir -p ~/clouddrive/Projects/tf-project
```

Now change to that new project folder.

```shell
cd ~/clouddrive/Projects/tf-project
```

In this project we will start separating resources into resource specific terraform files. We will still need a file called "main.tf" in this project folder this is where we will put the main terraform block:

```shell
touch main.tf
```

We also will need a for some resources related to a virtual machine that we will create in a future lab.  So we can create a file called `vm.tf` as a location to configure these new resources.  We will put resources closely coupled to the VM in that new file.

```shell
touch vm.tf
```

## Lab

Open the file “main.tf” and add the following 'terraform' and 'provider' blocks.  Notice that we are using a different `backend.azurerm.key` for the project state then the `tf-base` project. Here it's named "project.terraform.tfstate", specific to this particular project.
 

```
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
  }
  backend "azurerm" {
    resource_group_name  = "aztf-backend-rg"
    container_name       = "tfstate"
    key                  = "project.terraform.tfstate"
  }
  required_version = ">= 1.6.4"
}

provider "azurerm" {
  features {}
}
```

This Terraform configuration is set up to manage Azure resources using the Azurerm provider, ensuring it uses a specific version of the provider and Terraform itself. The state of the infrastructure managed by Terraform again is stored remotely in Azure Blob Storage, allowing team collaboration and remote operations without local state file constraints. This setup is typical for production or shared environments where managing state files locally would be risky or inconvenient.

This is a new project so we will need to run `terraform init` at this point in this project.  You again will need to have the storage account so Terraform can find the location to store the project state.  Terraform will prompt you for the storage account name. Type the same name used in the previous lab.

Run:

```shell
terraform init
```

---

Now that the foundation of the `tf-project` is in place we can create some resources in the project.  We will start with the `vm.tf` file for resources related to the "Bastion" Virtual Machine we will create in the next lab.  Have the value of the existing Public Subnet ID from the `subnet-public` output of the `tf-base` project handy as we will need it to create a network interface to be used by the "Bastion" Virtual Machine. 

In the `vm.tf` file we will define two Azure resources: a public IP address and a network interface. Both are commonly used components in setting up network connectivity for virtual machines or other cloud services that need to interact with the internet or other virtual networks.  Be sure to replace the `resource.azurerm_network_interface.lab-bastion.ip_configuration.subnet_id` value with the `subnet-public` output from the `tf-base` project.  The main section you will more then likely need to update is the subscription ID, 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX'.

```
resource "azurerm_public_ip" "lab-bastion" {
  name                = "aztf-labs-public-ip"
  resource_group_name = "aztf-lab-rg"
  location            = "eastus2"
  allocation_method   = "Dynamic"
  sku                 = "Basic"
  tags                = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}

resource "azurerm_network_interface" "lab-bastion" {
  name                = "aztf-labs-nic"
  resource_group_name = "aztf-lab-rg"
  location            = "eastus2"

  ip_configuration {
    name                          = "aztf-labs-app-ipconfig"
    subnet_id                     = "/subscriptions/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/resourceGroups/aztf-lab-rg/providers/Microsoft.Network/virtualNetworks/aztf-labs-vnet/subnets/aztf-labs-subnet-public"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.lab-bastion.id
  }

  tags = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}
```

### Public IP Resource
- **Resource Type**: `azurerm_public_ip`
- **Name**: `lab-bastion`
- **Properties**:
  - `name`: Specifies the name of the public IP address resource as "aztf-labs-public-ip".
  - `resource_group_name`: Indicates the Azure Resource Group under which this resource will be created, here "aztf-lab-rg".
  - `location`: Geographic location where the resource is deployed, specified as "eastus2".
  - `allocation_method`: Defines whether the IP address is statically or dynamically allocated. Here, it is set to "Dynamic", meaning the IP address may change if the resource is stopped and restarted.
  - `sku`: Specifies the pricing tier of the public IP address, set to "Basic".
  - `tags`: A set of key-value pairs used to organize and categorize the resource within Azure. In this case, tags denote the environment as "Lab" and the project name as "AZTF Training".

### Network Interface Resource
- **Resource Type**: `azurerm_network_interface`
- **Name**: `lab-bastion`
- **Properties**:
  - `name`: The name given to the network interface, "aztf-labs-nic".
  - `resource_group_name`: The Resource Group where this network interface is stored, which is the same as for the public IP address, "aztf-lab-rg".
  - `location`: The physical location for the network interface, matching the location of the public IP, "eastus2".
  
- **`ip_configuration`**:
  - `name`: Identifier for the IP configuration, "aztf-labs-app-ipconfig".
  - `subnet_id`: The Azure path that identifies the specific subnet where this network interface will operate. This is a detailed path that includes subscription ID, resource group, VNet, and the specific subnet. __Replace XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX with the appropriate subscription ID.__
  - `private_ip_address_allocation`: Indicates the allocation method for the private IP, set to "Dynamic", which allows Azure to assign a private IP automatically.
  - `public_ip_address_id`: Links this network interface to the public IP address created in the first resource block. It uses Terraform's resource reference syntax to dynamically fetch the ID of the public IP (`azurerm_public_ip.lab-bastion.id`).

- **`tags`**: Similar to the public IP, these tags help categorize the network interface within Azure. 

These two resources are typically used together to provide both public and private network connectivity to Azure services. The public IP allows services to be accessed from the internet, while the network interface connects Azure services to both internal (virtual network) and external (internet) resources. This setup is essential for creating accessible and secure configurations in cloud environments, especially when deploying resources that need to communicate over the internet or provide external-facing services.  We will use them later when creating the "Bastion" Virtual Machine.

Run `terraform validate`, `terraform plan`, and `terraform apply` troubleshooting as needed to create the two resources.

---

Look through the code to see what literal values are used more than once.  What do you come up with?  A general rule is to ignore the values in the terraform block because there you can not use locals or variables.  So look through the `vm.tf` file.

You may have noticed duplicate parameters with the same values in:
- `resource_group_name`
- `location`
- `tags`

While we are at it lets also include the Public `subnet_id` just incase that value is used somewhere else later.

Create a new `local.tf` file to store the "locals" in.

```shell
touch local.tf
```

In that file add the following "locals" block again replacing the Azure subscription ID in the `subnet_public` with the correct value.  This block defines a set of local values using the `locals` block in a Terraform configuration. Local values are a way to simplify and manage frequently used constants or expressions within your Terraform configurations.

```
locals {
  resource_group = "aztf-lab-rg"
  subnet_public = "/subscriptions/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/resourceGroups/aztf-lab-rg/providers/Microsoft.Network/virtualNetworks/aztf-labs-vnet/subnets/aztf-labs-subnet-public"
  region = "eastus2"
  common_tags = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}
```

### `locals` Block
- **Purpose**: The `locals` block is used to define variables that are local to your Terraform module. These are not exposed as inputs or outputs but are used internally within the module to simplify configuration and avoid repetition.

### Defined Local Values

1. **`resource_group`**:
   - **Value**: `"aztf-lab-rg"`
   - **Purpose**: This local value holds the name of the Azure Resource Group. By setting this as a local value, you can reference it elsewhere in your Terraform configurations without repeating the literal string, thus making your code cleaner and more maintainable.

2. **`subnet_public`**:
   - **Value**: A long Azure resource identifier (ID) string for a subnet, specifically `"aztf-labs-subnet-public"` within the virtual network `"aztf-labs-vnet"` and the resource group `"aztf-lab-rg"`.
   - **Purpose**: This stores the path to a specific subnet, which is useful when you need to reference this subnet in multiple resource configurations, such as when attaching network interfaces or configuring network security settings.

3. **`region`**:
   - **Value**: `"eastus2"`
   - **Purpose**: Specifies the Azure region where resources will be deployed. Using a local variable for this allows you to manage the deployment location centrally. If the region needs to be changed later, you can do so in just one place, and all resources referencing this local value will update accordingly.

4. **`common_tags`**:
   - **Value**: A map containing two key-value pairs:
     - `Environment = "Lab"`
     - `Project = "AZTF Training"`
   - **Purpose**: This local variable holds common metadata tags that you can apply to many Azure resources to ensure consistent tagging across resources, which helps with organization and cost tracking. Using locals for tags ensures that all resources have uniform tags and any change in tagging strategy can be implemented in one place.

### Usage
Local variables are referenced in other parts of the Terraform configuration using the syntax `local.<name>`, where `<name>` is the name of the local variable. For example, `local.resource_group` would be used to reference the `resource_group` local value.

By using local values in this way, you can make your Terraform configurations more modular, reusable, and easier to maintain. Changes in commonly used values such as resource group names, regions, or tagging strategies only need to be made once in the `locals` block instead of updating multiple instances across your codebase.

### Reference the new locals

Replace the `resource_group_name`, `location`, `tag`, and `subnet_id` values in the following resources with references in `vm.tf` to the new local values:
- resource.azurerm_public_ip.lab-bastion
- resource.azurerm_network_interface.lab-bastion

Remember you have `terraform validate` to help make sure your syntax is correct.  If you absolutely need, compare your `vm.tf` file to the code in the solution folder.

Run terraform plan:

```
terraform plan
```

Confirm that the plan does not come up with any changes to make to the actual infrastructure in Azure.
![Terraform plan results with locals declared](./images/tf-locals.png "Terraform plan results with locals declared")

If everything is done correctly and you have no changes, there is no need to run `terraform apply` at this time.

---

### Summary

#### Lab Objective
The objective of this lab is to introduce and implement the use of local values in Terraform to streamline configuration management by setting values that are frequently reused within the project. This helps in reducing repetition and errors, making the code more manageable and maintainable.

#### Lab Setup
Participants are required to set up a new project folder called `tf-project` where they will store all their Terraform configuration files for this exercise. This includes creating a `main.tf` for the main Terraform setup and a `vm.tf` for virtual machine-related resources.

#### Steps and Configurations
1. **Main Terraform Configuration**:
   - In `main.tf`, participants add a basic Terraform setup with the required providers, backend configuration for Azure, and the required version. This sets the stage for managing Azure resources using Terraform and specifying where the Terraform state file will be stored.

2. **Initialization**:
   - Run `terraform init` to initialize the Terraform environment in the new project directory. This step involves setting up the backend and downloading necessary provider plugins based on the configuration specified in `main.tf`.

3. **Resource Definition in `vm.tf`**:
   - Define resources related to a future "Bastion" virtual machine. This includes a public IP and a network interface, crucial for setting up network connectivity for VMs:
     - **Public IP Resource**: Configures a dynamically allocated public IP address.
     - **Network Interface Resource**: Sets up a network interface linked to the specified subnet and public IP, ensuring the VM can communicate within and outside the Azure network.

4. **Using Locals**:
   - Introduce a `locals` block in a new file `local.tf` to define commonly used values such as the resource group name, region, common tags, and the subnet ID. These values are referenced in the VM-related resources to reduce code duplication and centralize configuration changes.

#### Terraform Commands
- **`terraform validate`**: Ensures that the configurations are syntactically valid and internally consistent.
- **`terraform plan`**: Generates and shows an execution plan, confirming what actions Terraform will take based on the configurations provided.
- **`terraform apply`**: Applies the changes required to reach the desired state of the configuration.

#### Benefits of Using Locals
- **Simplification**: Locals simplify configurations by replacing repeated literals with a single, manageable point of definition.
- **Maintenance**: Easier to update configurations; changes in values like the region or resource group need to be made only once.
- **Clarity**: Improves the clarity of the Terraform code, making it easier to understand and audit.

#### Conclusion
This lab not only teaches the technical implementation of locals in Terraform but also encourages best practices in code organization and management. Participants learn to build more robust, scalable, and maintainable infrastructure as code environments by effectively utilizing Terraform's features like locals to manage cloud resources efficiently.
