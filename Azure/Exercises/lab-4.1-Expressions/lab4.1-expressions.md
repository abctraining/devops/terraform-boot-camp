# Expressions

Lab Objectives:
- Construct various types of expressions
- Use "terraform console" as development tool

## Lab

In this lab we will be using the “terraform console” command.  This opens the command line into a mode that allows you to type in expressions that are then evaluated.

Open Terraform console on the command line:

```
terraform console
```

At the “>” prompt, enter the following to see what they evaluate to.

#### Literal Values:

```
> 6.3

> ["foo", "bar", "baz"]   

> true    
```

<details>

 _<summary>Click to see results of above</summary>_

![Terraform console results](./images/tf-console-1.png "Terraform console results")
</details>

:information_source: Be careful when typing the quotes in the 2nd example above. Also, note that "True" will return an error in the 3rd example.

#### Conditional:

```
> local.common_tags["Environment"] == "Prod" ? "B_Gen4_1" : "GP_Gen5_4"

> local.region == "eastus2" ? "primary" : "secondary"
```
<details>

 _<summary>Click to see results of above</summary>_

![Terraform console results](./images/tf-console-2.png "Terraform console results")
</details>

#### Splat Expression:

```
> azurerm_network_security_group.lab-public.security_rule[*].destination_port_range
```

_Get a list of the ids for the subnets in a VNet:_

```
> azurerm_virtual_network.lab.subnet[*].id
```

<details>

 _<summary>Click to see results of above</summary>_

![Terraform console results](./images/tf-console-3.png "Terraform console results")
</details>

#### for expression:

_Extract the values of the common tags:_

```
> [for v in local.common_tags : v]
```

<details>

 _<summary>Click to see results of above</summary>_

![Terraform console results](./images/tf-console-4.png "Terraform console results")
</details>

<br /><br />
To exit the Terraform console, type:

```
> exit
```

---

### Summary of Terraform Lab: Expressions

#### Lab Objectives:
- Familiarize participants with the variety of expressions available in Terraform.
- Utilize the `terraform console` as a development tool to experiment with and understand how different expressions are evaluated within Terraform.

#### Lab Overview:
This lab is designed to help participants understand and practice with Terraform expressions in a live environment using the `terraform console`. The console acts as an interactive shell where users can type Terraform expressions and see their evaluated results immediately. This provides a hands-on learning experience and aids in debugging complex expressions.

#### Lab Activities:
Participants will perform a series of exercises within the Terraform console to explore different types of expressions:

### 1. **Literal Values**:
- Participants will test simple literals such as a floating number, an array of strings, and a boolean value to see how Terraform handles these basic types.
- **Example Expressions**:
  - `6.3`
  - `["foo", "bar", "baz"]`
  - `true`

### 2. **Conditional Expressions**:
- These exercises will introduce conditional logic, showing how to return different values based on the evaluation of a condition.
- **Example Expressions**:
  - Using tags to determine environment-specific settings (e.g., SKU names based on whether the environment is production).
  - Selecting labels based on region settings.

### 3. **Splat Expressions**:
- This will demonstrate how to succinctly access multiple elements of a list or map, especially useful for resources with multiple instances.
- **Example Expressions**:
  - Retrieving all destination port ranges from a list of network security rules.
  - Fetching IDs for all subnets within a virtual network.

### 4. **For Expressions**:
- Participants will explore the use of `for` expressions to transform and filter data structures in Terraform.
- **Example Expression**:
  - Extracting and listing values from common tags.

### Interactive Learning:
Using the `terraform console`, participants can directly interact with their Terraform configurations. This immediate feedback loop is invaluable for learning the effects of expressions and understanding data flow in Terraform:

- **Immediate Results**: Instant feedback on expression syntax and logic helps participants learn the correct usage and behavior of Terraform expressions.
- **Debugging Aid**: The console is an excellent tool for debugging expressions, particularly in complex modules and configurations.

#### Exiting the Console:
Participants are reminded that they can exit the Terraform console at any time by typing `exit`.

#### Educational Outcomes:
This lab emphasizes understanding and utilizing Terraform's expressive capability to manipulate and control infrastructure definitions dynamically. By the end of the lab, participants will be more comfortable crafting expressions to manipulate data and control resource configurations efficiently.

#### Conclusion:
Through practical exercises within the `terraform console`, this lab enhances participants' ability to write and debug Terraform expressions effectively, a critical skill for developing robust and dynamic infrastructure as code solutions.
