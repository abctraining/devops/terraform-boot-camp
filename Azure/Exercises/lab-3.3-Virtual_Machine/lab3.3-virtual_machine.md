# Virtual Machine

Lab Objectives:
- Create a virtual machine with a public IP
- See the effect of making a change to an existing resource

## Preparation

If you did not complete lab 3.2, you can simply copy the solution code from that lab (and run terraform apply) as the starting point for this lab.

## Lab

### Creating a Virtual Machine

The virtual machine we create in this lab is for a bastion host that has access from the public Internet.

Add a new resources to the `vm.tf` file.  These will reference the existing Public IP and Network interface from the previous lab.

1. A virtual machine. (For this lab we are allowing password access to the VM, but in practice you should use an SSH key instead for greater security.)

Here defines a resource for an Azure Linux Virtual Machine (VM) using the `azurerm_linux_virtual_machine` resource type. This block is set up to create a VM specifically configured with Ubuntu Server.

```
resource "azurerm_linux_virtual_machine" "lab-bastion" {
  name                  = "aztf-labs-bastion-vm"
  resource_group_name   = local.resource_group
  location              = local.region
  size                  = "Standard_B1s"
  network_interface_ids = [azurerm_network_interface.lab-bastion.id]
  admin_username        = "adminuser"
  admin_password        = "aztfVMpwd42"
  disable_password_authentication = false

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-confidential-vm-jammy"
    sku       = "22_04-lts-cvm"
    version   = "22.04.202210040"
  }

  tags = local.common_tags
}
```

### Resource Definition
- **Resource Type**: `azurerm_linux_virtual_machine`
- **Name**: `lab-bastion`
- **Properties**:
  - `name`: Specifies the name of the virtual machine as "aztf-labs-bastion-vm".
  - `resource_group_name`: Uses a local variable (`local.resource_group`) to specify the resource group under which the VM will be created. This local variable is defined elsewhere in the Terraform configuration to centralize and simplify the management of commonly used values.
  - `location`: Sets the geographic location of the VM to the value stored in another local variable (`local.region`).
  - `size`: Defines the size of the VM; here, it is set to "Standard_B1s", which is an economical option suitable for test or small workloads.
  - `network_interface_ids`: An array that contains the IDs of network interfaces associated with this VM. In this case, it includes the network interface defined by `azurerm_network_interface.lab-bastion.id`, linking this VM to a specific network interface configured in your infrastructure.
  - `admin_username`: Sets the administrative username for the VM as "adminuser".
  - `admin_password`: Specifies the password for the administrative account. Note: Hardcoding passwords in Terraform configuration files is not recommended for production environments due to security concerns. Consider using more secure methods like Azure Key Vault or environment variables.
  - `disable_password_authentication`: Indicates whether password authentication is enabled for the VM. Here, it is set to `false`, meaning password authentication is allowed.

### OS Disk Configuration
- **`os_disk` Block**:
  - `caching`: Sets the caching type for the operating system disk to "ReadWrite".
  - `storage_account_type`: Specifies the type of storage account for the OS disk; "Standard_LRS" indicates a standard locally-redundant storage.

### Source Image Reference
- **`source_image_reference` Block**:
  - `publisher`: The publisher of the VM image, here "Canonical", indicating an Ubuntu image.
  - `offer`: Specifies the offer name, "0001-com-ubuntu-confidential-vm-jammy".
  - `sku`: The stock-keeping unit, which in this case is "22_04-lts-cvm", specifying an Ubuntu 22.04 long-term support version.
  - `version`: Sets the version of the image to "22.04.202210040".

### Tags
- `tags`: Uses a local variable (`local.common_tags`) to apply a set of metadata tags to the VM, facilitating easier management and categorization of resources within Azure.

This Terraform block is structured to provision a Linux virtual machine in Azure with specific characteristics like size, region, network interface, and operating system details. It leverages local variables to make the configuration more modular and maintainable. The VM is configured with a basic security setup using password authentication, though for enhanced security, you might consider disabling password authentication and using SSH keys instead. This setup is ideal for scenarios where quick deployment and configuration of a VM are required, such as in development environments or for educational purposes like this exercise.

Run terraform validate to make sure you have no errors:
```
terraform validate
```

Run terraform plan.  See that three new resources will be created.
```
terraform plan
```

Run terraform apply:
```
terraform apply
```
![Terraform apply - VM creation](./images/tf-vm-apply.png "Terraform apply - VM creation")

### Try connecting to the virtual machine

Let's try checking that the infrastructure actually works by connecting to the virtual machine.

To connect to the virtual machine, you need its public IP.  You can get this in a couple ways:

1. Run terraform show

    a. Scroll up in the output to find the state for the virtual machine.  One of its attributes should be public_ip_address

    ![Public IP - Terraform show](./images/tf-show-vm-ip.png "Public IP - Terraform show")

2. Go to the Azure Portal

    a. In the portal search bar, type “public”.  Select the “Public IP addresses” auto-suggestion in the drop-down.

    b. Click on the public IP from the lab, and then see the IP address shown.

    ![Public IP - Azure portal](./images/az-vm-ip.png "Public IP - Azure portal")

<br /><br />
Once you find the public IP, SSH to the machine.  In the terminal console, type (substituting in the correct public IP):

```
ssh adminuser@<public-ip>
```
The ssh command will timeout and fail. (If it just hangs, then type ctl-c)  Can you figure out why it fails?   (Hint:  What is missing in the security group in main.tf?)   Let's fix that.

### Making Changes to An Existing Resource

To enable SSH access, we will need to add a security rule to enable TCP traffic in on port 22.

Add the following to the security group in the `resource.azurerm_network_security_group.lab-public` in the `tf-base/main.tf` file.  To see where to add the code, go to the Terraform documentation page for "azurerm_network_security_group". (Or you can look at the code in the solution folder of this lab.)

```
  security_rule {
    name                       = "SSH-Access"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefixes = azurerm_subnet.lab-public.address_prefixes
  }
```

Save the file and run terraform plan:
```
terraform plan
```

Notice that the plan shows an update to the security group.

![Terraform Plan - Added SG](./images/tf-plan-sg.png "Terraform Plan - Added SG")


Run terraform apply (remember to confirm yes to the changes):
```
terraform apply
```

When it finishes, try the ssh command again.  (You might need to wait a minute or two.)

This time it should prompt you for a password.  Enter the password that was configured in the vm.tf file.

*You may also be prompted to confirm that you want to connect. Enter "yes".*

Confirm you can ssh into the machine.

![SSH into VM](./images/cs-vm-ssh.png "SSH into VM")

Exit the SSH session on the virtual machine.

Also, do not forget to switch back to the `tf-project` folder for the next labs.

---

### Summary

#### Lab Objectives:
- **Primary Goal**: Create a Linux virtual machine in Azure with a public IP address.
- **Secondary Goal**: Demonstrate modifying an existing Terraform-managed resource by enabling SSH access through the addition of a network security rule.

#### Preparation:
Participants need to ensure that they have completed the necessary prerequisites or have the foundational setup from lab 3.2. They should use the configuration from that lab as the starting point, ensuring that the required network infrastructure (like public IP and network interface) is already in place.

#### Lab Details:

### Step 1: Creating a Virtual Machine
- **File**: The VM configuration is added to the `vm.tf` file.
- **Configuration**:
  - A `azurerm_linux_virtual_machine` resource is defined with Ubuntu Server, linked to a previously configured network interface and public IP.
  - The VM is set up with basic password authentication for simplicity in this educational context, though SSH keys are recommended for real-world applications.
  - System and network configurations include specifying resource group, location, VM size, and OS details.
  - The configuration uses local variables for resource group, location, and common tags to streamline and standardize settings across the project.

### Commands to Run:
- `terraform validate` — Ensure there are no configuration errors.
- `terraform plan` — Preview the changes Terraform will make, confirming that the VM and related resources will be created.
- `terraform apply` — Apply the configuration to create the VM and related infrastructure.

### Step 2: Testing the Setup
- **Connection Test**: Attempt to connect to the VM using SSH to verify that the public IP and VM are correctly configured. Initial attempts to connect should fail due to missing SSH access rules.

### Step 3: Modifying an Existing Resource
- **Objective**: Add a security rule to the network security group to allow inbound SSH traffic.
- **Update**: The security group's configuration is updated in the `tf-base/main.tf` file to include an inbound rule for TCP traffic on port 22.
- **Impact**: This modification demonstrates Terraform's ability to manage incremental changes without disrupting or recreating unrelated resources.

### Commands for Modifying Resources:
- `terraform plan` — Review the changes to be applied, specifically the addition of the SSH rule.
- `terraform apply` — Execute the plan to update the security group.

### Outcome:
- **Successful SSH Connection**: After updating the security rule, retrying the SSH connection should be successful, demonstrating both the VM's operational status and the effective update of network security settings.
- **Educational Insight**: This lab provides hands-on experience with managing and modifying Azure resources using Terraform, highlighting best practices in resource configuration and the agility of Terraform in infrastructure management.

#### Conclusion:
This lab not only teaches the basics of VM creation in Azure using Terraform but also emphasizes the importance of security configurations and the dynamic nature of infrastructure management with Terraform. Participants learn to deploy, verify, and modify cloud resources efficiently, gaining practical skills that are applicable to a wide range of cloud infrastructure scenarios.