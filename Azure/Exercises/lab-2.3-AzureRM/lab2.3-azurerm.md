# The AzureRM

Lab Objective:
- Add the AzureRM provider
- Create a resource group

## Preparation

If you did not complete lab 2.2, you can simply copy the code from that lab as the starting point for this lab.

## Lab

### Update Terraform Configuration:

#### Add the Azure Provider:

Open the file `main.tf` for edit.

We need to add the required Azure provider is specified in the Terraform block, you should include the 'hashicorp/azurerm' provider alongside the existing 'hashicorp/random' provider. Here's how you can modify the Terraform block with the required providers:

```
terraform {
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
  }
  required_version = ">= 1.6.4"
}
```

By adding the  `azurerm`  provider in the  `required_providers`  block, you ensure that Terraform will download and use the specified version of the Azure provider when managing Azure resources in your configuration.

Add the following Azure provider block to your Terraform configuration file. The AzureRM provider does need to have at least a blank `features` block:

```
provider "azurerm" {
  features {}
}
```

#### Create the Azure Resource Group:

Add the following resource block for a new `azurerm_resource_group` to your configuration.  We will explane more details about resource blocks later, for now you can find more information about this `azure_resource_group` in the documentation on the [Terraform Registry](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group):

```
resource "azurerm_resource_group" "backend" {
  name     = "aztf-backend-rg"
  location = "eastus2"
  tags     = {
    Environment = "Lab"
    Project     = "AZTF Backend"
  }
}
```

#### Save the Updated Configuration:

Save the file with the updated configuration that now includes the AzureRM provider and resource group blocks. This would also be a good time to run `terraform validate`.

```shell
terraform validate
```

Terraform may mention that you need to run `terraform init` first so it can validate your new HCL.

#### Initialize the Terraform Project:

Run `terraform init` in the directory containing your Terraform configuration file. 

```shell
terraform init
```

#### Plan the Changes:

Run `terraform plan` to review the execution plan and confirm the changes that Terraform will apply. 

```shell
terraform plan
```

#### Apply the Changes:

If the plan looks correct, run  terraform apply  to implement the changes and create the Azure resource group. 

```shell
terraform apply
```

### Summary:
In the previous lab section, you updated the Terraform configuration by adding the Azure provider and creating an Azure resource group. Here's a breakdown of the steps you followed:

1. **Added the Azure Provider:**
   - Modified the Terraform block to include the  `hashicorp/azurerm`  provider alongside the existing  `hashicorp/random`  provider. This ensures Terraform downloads and uses the specified version of the Azure provider for managing Azure resources.
   - Added the Azure provider block with a blank  `features`  block to the Terraform configuration file.

2. **Created the Azure Resource Group:**
   - Added a resource block for a new  `azurerm_resource_group`  to the configuration. The resource group was named "aztf-backend-rg" and set to the "eastus2" location with specific tags.
   - Referenced documentation on the Terraform Registry for more information about the  `azurerm_resource_group` .

3. **Saved the Updated Configuration:**
   - Saved the file with the updated configuration that now included the Azure provider and resource group blocks.
   - Ran  `terraform validate`  to ensure the syntax and configuration were correct, and initialized the Terraform project with  `terraform init` .

4. **Planned and Applied Changes:**
   - Reviewed the execution plan with  `terraform plan`  to confirm the changes that Terraform would apply.
   - Implemented the changes by running  `terraform apply`  to create the Azure resource group.

By following these steps, you successfully integrated the Azure provider and created an Azure resource group in your Terraform configuration. This summary captures the key actions taken in the lab section.