# Referencing Terraform Resources

Lab Objective:
- Add an Azure Storage Account
- Add an Azure Storage Container
- See how to reference terraform resources

## Preparation

If you did not complete lab 2.3, you can simply copy the code from that lab as the starting point for this lab.

## Lab

Open the file `main.tf` for edit.

### Update Terraform Configuration:

#### Add Azure Storage Resources:

Integrate two new resource blocks for  `azurerm_storage_account`  and  `azurerm_storage_container`  in your Terraform configuration file. These resources will create an Azure storage account and a storage container. Additionally, reference the existing  `random_integer`  resource to dynamically generate unique names.

```
resource "azurerm_storage_account" "backend" {
  name                     = "abcaztfbackend${random_integer.number.result}"
  resource_group_name      = azurerm_resource_group.backend.name
  location                 = azurerm_resource_group.backend.location
  account_tier             = "Standard"
  account_replication_type = "GRS"
}

resource "azurerm_storage_container" "backend" {
  name                  = "tfstate"
  storage_account_name  = azurerm_storage_account.backend.name
  container_access_type = "private"
}
```

Notice the references to Terraform attributes in the above resource blocks.

- Inside a string: `"abcaztfbackend${random_integer.number.result}"`
- Direct refference: `azurerm_storage_account.backend.name`

The list of arguments and attributes can be found in the documentation found on the Terraform registry: [azurerm_storage_account](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/data-sources/storage_account), [azurerm_storage_container](https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_container).

#### Add Output Block:

Include an output block to display the name of the created storage account.  We will explain these block types in detail later.

```
output "storage_account_name" {
  value = azurerm_storage_account.backend.name
}
```

### Save the Updated Configuration:

#### Save the File:

Save the file with the updated configuration that now includes the Azure storage resources and references to other Terraform resources.

### Validate and Apply Changes:

#### Validate Configuration:

Run  `terraform validate`  to ensure the syntax and configuration are correct.

```shell
terraform validate
```

#### Plan the Changes:

Run  `terraform plan`  to review the execution plan and confirm the changes that Terraform will apply.


```shell
terraform plan
```

#### Apply the Changes:

If the plan looks correct, run  `terraform apply`  to implement the changes and create the new Azure storage resources.

```shell
terraform apply
```

Take note of the value of the output, `storage_account_name`, this value will be needed in the next exercise.

### Summary:

In this lab section, you updated the Terraform configuration by adding Azure storage resources to create a storage account and a storage container. By integrating  `azurerm_storage_account`  and  `azurerm_storage_container`  resource blocks, you leveraged the existing  `random_integer`  resource to generate unique names dynamically. References to Terraform attributes within the resource blocks were demonstrated, both inside a string and directly.

You included an output block to display the name of the created storage account. The importance of referencing other Terraform resources was highlighted and will be explained further in detail later.

After saving the updated configuration file, you validated the syntax and configuration by running  `terraform validate` . Subsequently, you reviewed the execution plan using  `terraform plan`  to confirm the changes that Terraform will apply. Finally, by running  `terraform apply` , you implemented the changes to create the new Azure storage resources as defined in the updated configuration.