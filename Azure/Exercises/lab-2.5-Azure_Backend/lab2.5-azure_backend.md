# Using Azure Backend

Lab Objective:
- Save Terraform state to backend in Azure storage

## Preparation

If you did not complete lab 2.4, you can simply copy the code from that lab as the starting point for this lab.

Before we started this exercise, in this lab we will be using a second project folder in which we will create the base Azure resources (resource group, network, subnets) for the rest of the labs.  Create a project folder for this lab `~/clouddrive/Projects/tf-base`.

```shell
mkdir -p ~/clouddrive/Projects/tf-base
```

Now change to that new project folder.

```shell
cd ~/clouddrive/Projects/tf-base
```

Also you will need a file called "main.tf" in this project folder similar to the `tf-backend` project:

```shell
touch main.tf
```

Open the file for edit.  You can either use "vim" or you can use the built-in editor in the CloudShell".  To open the built-in editor, click the "Open Editor" icon (looks like curly braces) above the console area.  In the file editor, expand the directory tree on the left.

### Authenticate to Azure CLI

If you are running this lab in the Azure Cloud Shell, then Azure CLI authentication was already automatically done when you opened Cloud Shell.  

> If you are running this lab from a terminal shell outside of the Azure portal, then you would need to use the Azure CLI to authenticate by typing "az login" which will direct you to a browser login page to log into the Azure CLI.

## Lab

Add the following Terraform block that configures the requirements and settings for this Terraform project. We will be configuring a backend to store the terraform state in an Azure storage blob.  The storage account and container were in the previous labs.  The backend state will be stored in a new blob created in the container.

```
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
  }
  backend "azurerm" {
    resource_group_name  = "aztf-backend-rg"
    container_name       = "tfstate"
    key                  = "base.terraform.tfstate"
  }
  required_version = ">= 1.6.4"
}

provider "azurerm" {
  features {}
}
```

### Description

#### Terraform Block

1. **`required_providers`**:
   This section defines which providers are necessary for the Terraform configurations. In this case, it specifies the Azurerm provider from HashiCorp, along with a version constraint (`~> 3.96`). This constraint means that Terraform will use any version of the Azurerm provider that is compatible with version 3.96 but less than version 4.0.

   ```hcl
   required_providers {
     azurerm = {
       source  = "hashicorp/azurerm"
       version = "~> 3.96"
     }
   }
   ```

2. **`backend "azurerm"`**:
   This specifies that the state file of Terraform (where all the metadata regarding the resources managed by Terraform is stored) will be handled by Azure. Specifically:
   - `resource_group_name`: The name of the Azure Resource Group where the state file is stored.
   - `container_name`: The name of the Azure Storage Container in the Resource Group that will store the state file.
   - `key`: The name of the state file within the container.

   ```hcl
   backend "azurerm" {
     resource_group_name  = "aztf-backend-rg"
     container_name       = "tfstate"
     key                  = "base.terraform.tfstate"
   }
   ```

3. **`required_version`**:
   Specifies the minimum Terraform version required for the configurations. Here, it indicates that the project requires at least Terraform version 1.6.4 or higher.

   ```hcl
   required_version = ">= 1.6.4"
   ```

#### Provider Block

- **`provider "azurerm"`**:
  This section configures the Azure provider itself. The `features {}` block is required for the Azurerm provider but can be empty unless specific features need to be enabled or configured.

  ```hcl
  provider "azurerm" {
    features {}
  }
  ```

Now we can run `terraform init` to initialize a Terraform project in this working directory containing Terraform configuration files.

Executing `terraform init` is generally the first command you run in a new or updated Terraform configuration to ensure that Terraform's environment is correctly prepared for managing your infrastructure. This step is essential to align the local environment with the configuration's requirements including the AzureRM backend that we have configured.

A missing argument in the backend configuration above is the specification of an Azure storage account.  Terraform will therefore prompt you to enter the storage account name when you run terraform init.  The storage account name will be the value of the `storage_account_name` from the "outputs" in the `tf-backend` project from the previous labs.  The storage account name would look something like "abcaztfbackendXXXX" but with a random number replacing the "XXXX".  Enter that value when prompted when running `terraform init`.

Run:

```shell
terraform init
```

Terraform will prompt you for the storage account name. Type the name as per the instructions above.   

> If you enter the wrong storage account name, you will get an error.  Unfortunately you will not be able to just re-run terraform init.  You must first remove the .terraform subdirectory by typing "rm -rf .terraform".  You can then re-run terraform init.

Terraform will create a new state in Azure for this project.  The state will be saved in a new Azure storage blob referenced in the backend configuration above.

---

Now add this block to the `main.tf` file that defines a resource configuration for creating an Azure Resource Group using the Terraform Azure Resource Manager (Azurerm) provider.

```
resource "azurerm_resource_group" "base" {
  name     = "aztf-lab-rg"
  location = "eastus2"
  tags     = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}
```

### Description

#### Resource Block

- **`resource "azurerm_resource_group" "base"`**: This line declares a resource block. Terraform uses the term "resource" to manage infrastructure, such as an Azure Resource Group. 
  - `"azurerm_resource_group"` is the type of the resource, which in this case is a Resource Group within Azure.
  - `"base"` is the name you assign to this resource within your Terraform configuration. This name is used to refer to this resource in other Terraform blocks.

#### Configuration Arguments

- **`name = "aztf-lab-rg"`**: This specifies the name of the Resource Group as it will appear in Azure. In this case, the Resource Group will be named "aztf-lab-rg".

- **`location = "eastus2"`**: This determines the geographic location where the Resource Group will be created. Here, it's set to "eastus2", which corresponds to the East US 2 region in Azure.

- **`tags`**: This is a map of strings that provides a way to organize and categorize resources within Azure. Tags associated with the Resource Group are specified as key-value pairs:
  - `Environment = "Lab"`: A tag that can be used to specify that this Resource Group is part of a lab environment.
  - `Project = "AZTF Training"`: A tag that helps identify that this Resource Group is associated with the "AZTF Training" project.

Once the new 'azurerm_resource_group' is set in your Terraform project you can now add it to Azure by running `terraform plan` followed by `terraform apply`

Run:

```shell
terraform plan
```

Followed by:

```shell
terraform apply
```

If there where errors produced pay close attention to the outputs and use tools like `terraform validate` to help find where the problems could be in the Terraform project.

Once the `terraform apply` completes you should now be able to find the new Resource Group in the Azure portal.


You can also confirm the resources with terraform by checking the state use `terraform show`.

```shell
terraform show
```

---

### Summary:

The primary goal of this lab is to teach participants how to configure Terraform to store its state file in an Azure backend, specifically within an Azure storage blob.

#### Preparation Steps
- Participants are advised to create a new project folder (`~/clouddrive/Projects/tf-base`) and navigate into it. They need to prepare a file named `main.tf` which will hold all the Terraform configurations.

#### Azure Authentication
- For users within the Azure Cloud Shell, authentication is automatic. Those using an external terminal must manually authenticate using the `az login` command.

#### Configuration Details
- **Terraform Block**: Setup involves declaring the required AzureRM provider version, specifying the backend configuration for Azure with details about the resource group, storage container, and state file key, and setting the required Terraform version.
- **Provider Block**: The AzureRM provider is configured with minimal setup, requiring only an empty `features` block.

#### Execution Instructions
- **Initialization**: Run `terraform init` to initialize the Terraform environment, which will set up the backend and install necessary providers. During this, users will be prompted to enter the Azure storage account name manually.
- **Resource Definition**: A Terraform block is added to define an Azure Resource Group with specified attributes like name, location, and tags.
- **Applying Configuration**: Use `terraform plan` to preview changes and `terraform apply` to create resources in Azure.
- **Verification**: After applying, the state of the infrastructure can be verified with `terraform show`.

#### Error Handling
- If there are errors due to incorrect backend details (like the storage account name), users need to clear the `.terraform` directory before retrying the initialization.

This lab guides users through setting up a Terraform configuration for managing Azure resources with an emphasis on backend state management to ensure a reliable and scalable infrastructure management practice.
