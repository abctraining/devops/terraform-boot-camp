# Functions

Lab Objective:
- Use functions to merge tags and determine VM cluster size

## Preparation

If you did not complete lab 4.5, you can simply copy the solution code from that lab (and do terraform apply) as the starting point for this lab.  This lab also will be back in the `tf-project` folder.

## Lab

### Tag Merge

Open the file `variables.tf`.

Add a new variable to accept additional tags to set on our resources.  What should be the type for the tags variable?  (Hint: take a look at the existing tags local value)  Declare an empty map as the default value for the variable.

<details>

 _<summary>Click to see solution for tags variable</summary>_

```
variable "tags" {
  type = map(string)
  default = {}
}
```
</details>

Open the file `local.tf`.

Modify the common_tags local value to merge the tags input variable and the existing tag map.  What function would you use?  Try your hand at writing the solution before checking the solution below.  You can use terraform validate to check the syntax correctness of your solution.

<details>

 _<summary>Click to see solution for merging tags variable and local value</summary>_

```
  common_tags = merge(var.tags,{
    Environment = "Lab"
    Project     = "AZTF Training"
  })
```
</details>

Run `terraform validate`:

```shell
terraform validate
```

Run `terraform plan` and confirm that nothing needs to be updated.  Why is that?

```shell
terraform plan
```

Now open the `terraform.tfvars` file.  Add a value for tags variable:

```
tags = {
  Owner = "Development"
}
```

Run `terraform plan` and see that the tags for numerous resources will be updated:

```shell
terraform plan
```

![Terraform Plan - Add value for Owner and merge tags](./images/tf-plan-merge.png "Terraform Plan - Add value for Owner and merge tags")

Run `terraform apply`:
```
terraform apply
```

### Cluster Size Calculation

Now let’s try some more functions.

Open the file `local.tf`.

Add a lookup map as follows in the locals block directly above the `cluster_size` local:

```
  size_spec = {
    low = {
      cluster_size = 1
    },
    medium = {
      cluster_size = 2
    },
    high = {
      cluster_size = 3
    }
  }
```

Add two more variables to variables.tf:

* **node_count** of type number with default as null

* **load_level** of type string with default as empty string

<details>

 _<summary>Click to see solution for variables node_count and load_level</summary>_

```
variable "node_count" {
  type = number
  default = null
}

variable "load_level" {
  type = string
  default = ""
}
```
</details>

We now want a way to determine `cluster_size` according to the following criteria:
*	Use input variable node_count if it is not null
*	Otherwise use the input variable load_level to lookup a cluster_size value from the size_spec map
*	If both node_count and load_level input variables are undefined, then the default cluster size should be 1

:question: Can you think of what this would look like?

*Hint: It could be a combination of coalesce and lookup, maybe with try.*

Try your hand at writing out the computation.  Replace the current hard-coded local value of `cluster_size` with the computation. That should be set in your `local.tf` file. Compare your solution to that below (or in the `tf-project/local.tf` file in the solution folder).

<details>

 _<summary>Click to see one solution for computing cluster_size</summary>_

```
  cluster_size = try(coalesce(var.node_count, lookup(local.size_spec,var.load_level).cluster_size), 1)
```
</details>

Run `terraform validate`:

```shell
terraform validate
```

Run `terraform plan`:

```shell
terraform plan
```

:question: What is the effect in the execution plan on the number of application nodes? What does that tell you about the computed cluster size and how that value was derived?

Now edit the `terraform.tfvars` file to add the one of the input variables.

* Set the value of `load_level` to “high”.

Run `terraform plan`:

```shell
terraform plan
```

:question: What is the effect in the execution plan on the number of application nodes? What does that tell you about the computed cluster size and how that value was derived?

Add another setting in `terraform.tfvars`:

* Set the value of `node_count` to 2.

Run `terraform plan`:

```shell
terraform plan
```

:question: What is the effect in the execution plan on the number of application nodes? What does that tell you about the computed cluster size and how that value was derived?

You should have seen the following behavior (based on there being two VMs in the cluster to start with):
* When the `load_level` and `node_count` are both missing from `terraform.tfvars`, the `cluster_size` derivation should have used the default value of `1`, and the plan should have shown one virtual machine (plus its network interface and backend address pool association) to be destroyed.
![Terraform Plan - missing load_level and node_count](./images/tf-plan-cluster1.png "Terraform Plan - missing load_level and node_count")
* When you set `load_level` to "high" in `terraform.tfvars`, the `cluster_size` derivation should have used the lookup value of `3`, and the plan should have shown one virtual machine (plus its network interface and backend address pool association) to be created.
![Terraform Plan - load_level = high](./images/tf-plan-cluster2.png "Terraform Plan - load_level = high")
* When you set the `node_count` to `2` in `terraform.tfvars`, the `cluster_size` derivation should have used that value, and the plan should have shown no change in the number of virtual machines.
![Terraform Plan - node_count = 2](./images/tf-plan-cluster3.png "Terraform Plan - node_count = 2")

## Bonus

There is an interesting quark if you used the provided solution to calculate the `cluster_size`: as it is written `node_count` is ignored completely if `load_level` is not set.  You can verify this by removing the `load_level` from the `terraform.tfvars` file.  Go ahead and also make sure `node_count` is not set in the `terraform.tfvars` file as we will pass the variable values in at run time with the '-var' option.  Now when you do not have values for `load_level` or `node_count` set in `terraform.tfvars` a `terraform plan` should result in the `cluster_size` being set to `1`.

```shell
terraform plan
```

Now we should be able to demonstrate the quirk simply by testing with `terraform plan` setting the `node_count` value at plan time.

```shell
terraform plan -var='node_count=1'
```

```shell
terraform plan -var='node_count=2'
```

```shell
terraform plan -var='node_count=3'
```

All three examples above return the same result.  Why is that? See if you can figure that out.  If so can you improve the calculation for `cluster_size`?

Here are a few more quick tests to verify with.

```shell
terraform plan -var='load_level=high'
```

```shell
terraform plan -var='node_count=2' -var='load_level=high'
```

If you figure it out.  Share the reason and or solution with the Instructor.

---

### Summary

#### Lab Objective:
This lab focuses on enhancing participants' understanding of Terraform functions to perform complex operations such as merging tags and determining the size of a virtual machine (VM) cluster dynamically based on input parameters.

#### Preparation:
Participants should have a Terraform setup from lab 4.5 ready. This lab takes place in the `tf-project` directory, requiring the use of local values and various functions to manage resource configurations dynamically.

#### Lab Steps:

### Step 1: Tag Merge
- **Variable Addition**: Participants are instructed to add a new variable in `variables.tf` to accept additional tags, ensuring that it can store a map of strings.
- **Local Variable Modification**: The `common_tags` local variable in `local.tf` is modified to merge the newly added variable with existing tags using the `merge` function. This allows any additional tags specified during runtime to be combined with the predefined tags.
- **Validation and Execution**: After modifying the tags, participants run `terraform validate` to check syntax, and `terraform plan` to ensure that existing resources don't require updates unless new tags are added via `terraform.tfvars`.

### Step 2: Cluster Size Calculation
- **Local Variable Addition**: New local variables for `node_count` and `load_level` are introduced in `variables.tf` to allow dynamic cluster sizing.
- **Lookup Map**: A `size_spec` map is defined in `local.tf`, mapping different load levels to corresponding cluster sizes.
- **Complex Local Value Calculation**: The `cluster_size` local variable is recalculated using a combination of `try`, `coalesce`, and `lookup` functions to dynamically determine the cluster size based on `node_count` or `load_level`.
- **Implementation and Testing**: Participants update `terraform.tfvars` to test various scenarios by setting `node_count` and `load_level`. They use `terraform plan` to observe how the cluster size adjusts based on these inputs.

#### Key Functions and Concepts Explored:
- **Merge Function**: Used to combine two maps of tags, demonstrating how to dynamically add or override resource tags.
- **Coalesce Function**: Employed to select the first non-null value from a list of possibilities, here used for deciding the cluster size.
- **Lookup Function**: Utilized within the context of a map to find values based on keys, demonstrating conditional logic in resource configurations.
- **Try Function**: Showcases error handling by attempting a lookup and providing a default if the lookup fails.

#### Educational Outcomes:
- **Dynamic Configuration Management**: Participants learn to use Terraform functions to make their infrastructure configurations more dynamic and adaptable to input changes, reducing hardcoding and improving reusability.
- **Advanced Function Usage**: The lab provides a practical application of advanced Terraform functions, enhancing participants' ability to write sophisticated expressions that respond to varying infrastructure requirements.
- **Practical Use Cases**: Through exercises like tag merging and dynamic cluster sizing, participants see firsthand how such functions can be applied to real-world scenarios, such as managing environment-specific configurations and scaling resources according to demand.

#### Conclusion:
This lab demonstrates the power of Terraform functions, enhancing participants' skills in managing infrastructure as code with greater flexibility and precision. By integrating these functions into their Terraform workflows, participants can ensure their configurations are both scalable and easily maintainable, aligning with best practices in cloud infrastructure management.
