# Creating an Azure Redis Cache using Terraform Module

Lab Objective:
- Utilize the Terraform module to create an Azure Redis Cache instance.

## Preparation

If you did not complete lab 4.6, you can simply copy the solution code from that lab (and do terraform apply) as the starting point for this lab.

## Lab

Explore the Terraform Registry to understand the available modules for Azure Redis Cache. Specifically, we will use the module from:

* [Claranet Redis Module on Terraform Registry](https://registry.terraform.io/modules/claranet/redis/azurerm/7.7.0)

Select version 7.7.0 for this lab.

Review the module documentation to understand its usage. Pay special attention to the required and optional input arguments. This module provides an abstraction over the Azure Redis Cache resource, simplifying its creation and configuration.

Create a new file named `redis.tf` in your Terraform project.

Using the module documentation as a reference, add a module configuration to create an Azure Redis Cache instance. Although this module supports various configurations, we will focus on a basic setup for this lab.

Key points for the module configuration:

* Explicitly specify the version as "7.7.0".
* Use inputs from your current Terraform configuration where applicable, ensuring consistency with your existing resources.
* Append a unique suffix to the `client_name` for the Redis instance to avoid naming conflicts.
* Exclude settings from the module that were not present in your existing resources.

After setting up the module, compare your code to the solution provided below.

<details>
<summary>Click to see the solution for the Redis module configuration</summary>

```hcl
module "redis" {
  source  = "claranet/redis/azurerm"
  version = "7.7.0"

  client_name              = "aztf-labs-redis-${random_integer.suffix.result}"
  environment              = "labs"
  location                 = local.region
  location_short           = "use"
  stack                    = "labs"
  resource_group_name      = local.resource_group
  logs_destinations_ids    = []

  capacity                 = 1
  cluster_shard_count      = 2
  data_persistence_enabled = false
  allowed_cidrs            = ["10.0.0.0/16"]

  extra_tags               = local.common_tags
}
```
</details>

### Terraform Commands

1. Initialize your Terraform configuration:
   ```shell
   terraform init
   ```
2. Validate your configuration:
   ```shell
   terraform validate
   ```
3. Plan and review the changes:
   ```shell
   terraform plan
   ```
4. Apply the changes:
   ```shell
   terraform apply
   ```

:bangbang: NOTE: Azure takes a while to create the Redis cache.  Expect it to be 10 minutes or more.

### Discussion Questions

1. Why is `terraform init` necessary when adding a new module?
2. How does using a module simplify the process of creating an Azure Redis Cache?
3. What are the benefits of appending a unique suffix to resource names in a shared environment?

---

### Summary

#### Lab Objective
This lab focuses on leveraging a Terraform module to create and configure an Azure Redis Cache instance, introducing the concept of using community-driven modules from the Terraform Registry to streamline the deployment of cloud resources.

#### Preparation
Participants are expected to use their existing Terraform setup from the previous lab as a foundation, ensuring continuity in their learning and application of Terraform's capabilities.

#### Lab Activities

### Step 1: Research and Selection of the Module
- **Module Discovery**: Participants explore the Terraform Registry to find a suitable module for deploying Azure Redis Cache. The chosen module for this lab is the Claranet Redis Module.
- **Module Review**: Participants are encouraged to review the documentation of the selected module to understand its capabilities, requirements, and potential configurations. This includes both mandatory and optional inputs as well as outputs provided by the module.

### Step 2: Configuration of the Redis Module
- **File Creation**: Participants create a new Terraform file named `redis.tf`, dedicated to configuring the Redis Cache using the module.
- **Module Implementation**:
  - **Version Specification**: The module version is explicitly set to "7.7.0" to ensure compatibility and predictability in the resources it provisions.
  - **Resource Configuration**: Using inputs from the existing Terraform setup, participants configure the Redis Cache. This includes setting a unique client name by appending a random suffix, specifying the geographic location, resource group, and other relevant parameters.
  - **Simplification**: By using the module, participants avoid the complexities involved in manually configuring each aspect of the Azure Redis Cache, as the module abstracts much of this complexity.

### Step 3: Execution of Terraform Commands
- **Initialization (`terraform init`)**: Required to prepare Terraform to manage the new module, including downloading necessary plugins and modules.
- **Validation (`terraform validate`)**: Ensures the Terraform configuration is syntactically correct and ready for execution.
- **Planning (`terraform plan`)**: Allows participants to review the planned actions before any changes are applied to the infrastructure.
- **Application (`terraform apply`)**: Executes the plan to create resources in Azure, notably the Redis Cache instance, which may take significant time due to Azure's operational procedures.

#### Key Learning Points
1. **Module Usage**: Demonstrates the practical benefits of using modules from the Terraform Registry, highlighting how they can reduce the complexity and increase the reliability of infrastructure provisioning.
2. **Version Control**: Emphasizes the importance of specifying module versions to avoid unexpected changes that might come from updates to the module.
3. **Consistency and Scalability**: Shows how to integrate modules with existing configurations to maintain consistency across the infrastructure and scale up operations efficiently.

#### Discussion Questions Explained
1. **Importance of `terraform init`**: Necessary for incorporating new modules into the project, as it initializes the use of any new providers or modules not previously configured.
2. **Module Benefits**: Using a module simplifies setup by encapsulating detailed resource configurations into a reusable, standardized component that can be easily integrated into broader Terraform projects.
3. **Unique Suffixes**: Adding unique suffixes to resource names helps prevent naming conflicts in shared environments, ensuring that resources are distinctly identifiable and reducing the risk of unintended interactions between deployments.

### Conclusion
This lab provides a comprehensive introduction to using Terraform modules for cloud resource deployment, with a focus on creating an Azure Redis Cache. It teaches the value of modular infrastructure code, the efficiency of using community-supported modules, and the best practices in deploying resources in a cloud environment using Terraform.
