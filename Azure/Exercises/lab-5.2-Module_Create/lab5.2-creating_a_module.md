# Creating a Module

Lab Objective
- Convert the load balancer configuration in your code to a module
- Use the new module in your configuration

## Preparation

If you did not complete lab 5.1, you can simply copy the solution code from that lab (and do terraform apply) as the starting point for this lab.

## Lab

### Modify code to implement the module

In this lab we will convert the load balancer configuration to be a module implementation.  We will implement the module as a nested module, though in actual practice this module should probably be a module on its own.

Create a subdirectory called `load-balancer`.

```shell
mkdir load-balancer
```

#### Load balancer main

Move the `lb.tf` file to the `load-balancer` directory and rename the file `main.tf`.  (Recall that each module should have a `main.tf` file as the principal configuration entry point.)  Let's make a couple changes to the file.

1. First, a module should include its provider requirements.  So add the following to the top of the load balancer `main.tf`.  (Note that we do not add a backend specification or a provider block.)

```
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.86"
    }
  }
  required_version = ">= 1.3.0"
}
```

##### `terraform` Block
This block is used to configure behavior that applies to Terraform itself, including provider requirements and the minimum required Terraform version.

##### `required_providers` Sub-block
This sub-block specifies which providers are necessary for the Terraform configurations. Providers are plugins or software components that Terraform uses to interact with remote systems. Each provider offers a series of resource and data source types that correspond to specific features of a cloud or service.

- **Provider `azurerm`**:
  - `source`: Specifies the location or name of the provider in the Terraform Registry. Here, `hashicorp/azurerm` indicates that the Azure provider is maintained by HashiCorp and is available in the Terraform Registry. This source tells Terraform where to find the provider.
  - `version`: Indicates the version of the provider that Terraform should use. The notation `"~> 3.86"` is a version constraint that means Terraform will use any version that is compatible with version 3.86 but less than version 4.0. This allows automatic upgrades to newer versions as long as they do not include breaking changes according to Semantic Versioning.

##### `required_version` Directive
This directive specifies the minimum version of Terraform that is required to execute this configuration. The value `">= 1.3.0"` means that the configuration will only work with Terraform version 1.3.0 or any newer version. This ensures that all the features used in the Terraform configuration are supported by the version of Terraform being used.

This Terraform block is for ensuring that the right versions of Terraform and the Azure RM provider are used in the modules, which helps prevent issues related to version incompatibilities.

2. Second, to avoid a name collision later when you do terraform apply, change the Azure name of the following load balancer resources by adding a "mod-" prefix:
  * azurerm_public_ip:  change name from "aztf-labs-lb-public-ip" to "mod-aztf-labs-lb-public-ip"
  * azurerm_lb: change name from "aztf-labs-loadBalancer" to "mod-aztf-labs-loadBalancer"
  * azurerm_lb_rule:  change name from "aztf-labs-lb-rule" to "mod-aztf-labs-lb-rule"

#### Load balancer variables

Within the `load-balancer` directory, create a file called `variables.tf`.

Go through the **load balancer main.tf** file and look for what arguments will need values passed into the module.  (The load balancer module cannot access the parent resources directly.)  These are candidates for the input variables for the load balancer module.

In the load balancer `variables.tf` file, add variables for the following:
  * location
  * resource group name
  * tags

Try to write the variables.tf code on your own initially. Compare your code to the solution below (or in the `load-balancer/variables.tf` file in the solution folder).

<details>

 _<summary>Click to see solution for load balancer module variables</summary>_

```
variable "location" {
  type = string
}

variable "resource_group_name" {
  type = string
}

variable "tags" {
  type = map(string)
}
```
</details>

Open the `load-balancer/main.tf` file and use these variables to populate the corresponding arguments in all of the resources in the file.

#### Load balancer outputs

Within the `load-balancer` directory, create a file called `outputs.tf`.

Go through the root module's files to see where load balancer attributes are referenced.  These are candidates for output values from the load balancer module.

In the load balancer outputs.tf file, add outputs for the following:
  * load balancer backend address pool id
  * load balancer public ip address

Try to write this on your own initially.  Compare your code to the solution below (or in the `load-balancer/outputs.tf` file in the solution folder).

<details>

 _<summary>Click to see solution for load balancer module outputs</summary>_

```
output "backend_address_pool_id" {
  value = azurerm_lb_backend_address_pool.lab.id
}

output "public_ip_address" {
  value = azurerm_public_ip.lab-lb.ip_address
}
```
</details>

At this point, you now have a nested module with inputs and outputs defined.  Next, let's use the new module.

### Modify code to call the new module

Open the file `vm-cluster.tf` in the root module.  Add a call to the load balancer module, setting argument values corresponding to the input variables for the load balancer.  The module source should be `./load-balancer`.

Try writing this on your own first. Compare your code to the solution below (or in the `vm-cluster.tf` file in the solution folder).

<details>

 _<summary>Click to see solution for calling load balancer module</summary>_

This Terraform block defines a module instantiation within a Terraform configuration.

```
module "load-balancer" {
  source = "./load-balancer"

  location            = local.region
  resource_group_name = local.resource_group
  tags                = local.common_tags
}
```

##### `module "load-balancer"`
- **Keyword**: `module`
- **Name**: `load-balancer`
- **Purpose**: This is the declaration of a module within the Terraform configuration. Modules in Terraform are containers for multiple resources that are used together. This modular approach allows for reusing common configurations and simplifies management by grouping related resources. The name `"load-balancer"` is an identifier used within the configuration to refer to this module instance.

##### Attributes of the Module:

- **`source = "./load-balancer"`**
  - **Attribute**: `source`
  - **Value**: `"./load-balancer"`
  - **Description**: Specifies the location of the module. The value `./load-balancer` indicates that the module's source code is located in a directory named `load-balancer` relative to the location of the Terraform configuration file that includes this module. This directory will contain its own set of Terraform configuration files defining resources, variables, outputs, etc., that make up the load balancer module.

- **`location = local.region`**
  - **Attribute**: `location`
  - **Value**: `local.region`
  - **Description**: Passes the value of the `local.region` variable to the module. Locals are usually defined in the Terraform configuration to simplify management and increase readability. Here, `local.region` likely stores the geographic region where the resources should be deployed, and it is provided to the module to specify where the load balancer should be created.

- **`resource_group_name = local.resource_group`**
  - **Attribute**: `resource_group_name`
  - **Value**: `local.resource_group`
  - **Description**: Similar to the `location`, this attribute passes the name of a resource group, stored in `local.resource_group`, to the module. The resource group is a logical grouping of resources in cloud platforms like Azure, which helps organize and manage the lifecycle of resources that belong together.

- **`tags = local.common_tags`**
  - **Attribute**: `tags`
  - **Value**: `local.common_tags`
  - **Description**: Provides a map of tags (key-value pairs) defined in `local.common_tags` to the module. Tags are used to organize, manage, and categorize resources within a cloud environment. Passing common tags to the module ensures that all resources created within the module are consistently tagged, which can be important for cost allocation, governance, and operational management.

This Terraform module block instantiates a load balancer module, providing it with configuration values such as location, resource group, and tags from the parent configuration. This allows the load balancer resources to be configured with consistent settings that align with the overall infrastructure setup, ensuring that they are deployed in the correct region, within the right resource group, and tagged according to the organization's standards. The modular approach encapsulates the load balancer configuration, making the overall Terraform setup more organized and maintainable.

</details>

In the root module, you now need to use the module outputs to replace references to the load balancer attributes.  Be sure to use the "module" prefix in the references.

* Update the reference to backend_address_pool_id in vm-cluster.tf
* Update the reference to load-balancer-public-ip in outputs.tf in the root module.

### Execute terraform commands

To run the terraform commands, you must be in the root module's directory.  :bangbang: **Verify you are in the root module folder.**  If not, move to that directory.

Let's now validate the code you've written.  If you run `terraform validate` at this point, you will get an error that you need to run `terraform init` first.  Do you recall why this is necessary?

Run `terraform init`.

```shell
terraform init
```

Run `terraform validate` and fix errors as appropriate.

```shell
terraform validate
```

Run `terraform plan`. You will see that Terraform wants to replace the load balancer and various ancillary resources.

```shell
terraform plan
```

![Terraform Plan - LB Module](./images/tf-plan-lb-module1.png "Terraform Plan - LB Module")

![Terraform Plan - LB Module](./images/tf-plan-lb-module2.png "Terraform Plan - LB Module")


Run `terraform apply`:
> If you get an error that a resource was not able to be re-created since its predecessor was not yet deleted (i.e., a name conflict), then you may have missed renaming a few resources earlier in this lab.  You should be able to just re-run terraform apply again since the conflicting resources should have been fully destroyed by the conclusion of the first apply.

```shell
terraform apply
```

### (Optional) Trying out your infrastructure

If you have extra time now or later, you can verify that the load balancer actually works to connect to the clustered VMs.  See the instructions at [Testing Your Cluster](../optional-material/testing_your_cluster.md).  If you already set up the HTTP servers before, you should be able to just hit the load balancer public IP again now.

## Lab Cleanup

This is the final lab of the class.  When you are done with the lab and are satisfied with the results, please tear down everything you created by running terraform destroy:

```shell
terraform destroy
```

You might get an error about not having permission to perform a purge action on the key vault.  If so, just run terraform destroy again.

---

### Summary

#### Lab Objective:
This lab focuses on converting an existing load balancer configuration into a reusable Terraform module. This modular approach allows for better organization, reusability, and management of Terraform configurations.

#### Preparation:
Participants should start with the configuration from the previous lab (5.1) and work within the `tf-project` directory.

#### Steps of the Lab:

### Step 1: Module Setup
- **Directory Creation**: Participants create a new directory named `load-balancer` intended to contain all module files.
- **File Movement and Renaming**: The existing `lb.tf` file is moved into the new directory and renamed to `main.tf` to serve as the main entry for the module.
- **Module Configuration**:
  - A `terraform` block is added at the top of the `main.tf` file specifying the required provider (`azurerm`) and Terraform version.
  - Resource names within the module are prefixed with "mod-" to prevent naming collisions during resource re-creation.

### Step 2: Define Module Variables and Outputs
- **Variables (`variables.tf`)**: Variables for `location`, `resource_group_name`, and `tags` are defined to allow external configuration of these parameters when the module is used.
- **Outputs (`outputs.tf`)**: Outputs for the load balancer's backend address pool ID and public IP address are defined to make these values accessible to other parts of the Terraform configuration.

### Step 3: Using the Module
- **Module Integration**: In the `vm-cluster.tf` file in the root module, the new `load-balancer` module is instantiated and configured using the previously defined variables.
- **Adjustment of Root Module**: The root module's references to load balancer attributes (like backend address pool ID and public IP) are updated to use the outputs from the newly created `load-balancer` module.

### Step 4: Terraform Execution
- **Initialization (`terraform init`)**: Necessary to prepare the configuration for use, including initializing the new module and downloading required provider plugins.
- **Validation (`terraform validate`)**: Ensures that the module and its integration are correctly configured without syntax errors.
- **Planning and Applying (`terraform plan` and `terraform apply`)**: Participants observe the planned changes, which include the replacement of the old load balancer configuration with the new modular configuration, and apply these changes to their Azure environment.

#### Educational Outcomes:
- **Modularity**: Participants learn how to abstract a part of their Terraform configuration into a module, promoting reuse and simplification of their main configuration.
- **Dynamic Configuration**: By parameterizing the module with variables and exposing important attributes via outputs, participants can dynamically configure modules and integrate their outputs with other parts of their infrastructure.
- **Practical Implementation**: The lab provides a hands-on approach to modularizing infrastructure, which is crucial for managing complex Terraform projects and promoting best practices in infrastructure as code.

#### Conclusion:
This lab effectively demonstrates the conversion of a specific infrastructure component (a load balancer) into a reusable module. This modular approach not only makes the Terraform configuration cleaner and more organized but also enhances its maintainability and scalability. By the end of the lab, participants will have a solid understanding of how to create, configure, and integrate Terraform modules, equipping them with skills applicable to a wide range of real-world infrastructure scenarios.
