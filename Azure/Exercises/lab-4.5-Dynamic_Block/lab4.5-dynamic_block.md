# Dynamic Blocks

Lab Objective:
- Implement a dynamic block to handle multiple security group rules

## Preparation

If you did not complete lab 4.4, you can simply copy the solution code from that lab (and do terraform apply) as the starting point for this lab.

## Lab

This lab we will be modifying the network security groups created in the last lab in the `tf-base` project.  In that project open the `vm-cluster-security.tf` for edit.

Notice that within the network security group resource for the private subnet, there are multiple security group rule sub-blocks.  We will replace the multiple sub-blocks by a single dynamic block.

A dynamic block uses the for_each construct, which you now know requires a map of values by which to populate values for each iteration.  Since there are two security group rules, the map will have two keys.  What might you use as the map key for the different security rules?

Add a 'locals' block and add a new map with two keys (use the security rule names as the keys) and a sub-map for each key to specify the following values:
* priority
*	direction
*	access
*	protocol
*	destination_port

Try your hand at writing the map before looking at the solution below (or in main.tf in the solution directory).

<details>

 _<summary>Click to see solution for security group map</summary>_

```
  sg_rules = {
    HTTP-Access = {
      priority               = 100,
      direction              = "Inbound",
      access                 = "Allow",
      protocol               = "Tcp",
      destination_port_range = 80
    },
    SSH-Access = {
      priority               = 110,
      direction              = "Inbound",
      access                 = "Allow",
      protocol               = "Tcp",
      destination_port_range = 22
    }
  }
```
</details>

Now replace the security group rules in the private network security group with a dynamic block.  Try your hand at it, then compare your code to the solution below (or in main.tf in the solution folder).

<details>

 _<summary>Click to see solution for dynamic block</summary>_

```
  dynamic "security_rule" {
    for_each = local.sg_rules
    content {
      name                         = security_rule.key
      priority                     = security_rule.value.priority
      direction                    = security_rule.value.direction
      access                       = security_rule.value.access
      protocol                     = security_rule.value.protocol
      source_port_range            = "*"
      destination_port_range       = security_rule.value.destination_port_range
      source_address_prefix        = "*"
      destination_address_prefixes = azurerm_subnet.lab-private.address_prefixes
    }
  }
```

### Dynamic Block
- **Keyword**: `dynamic`
- **Block Label**: `security_rule`
- **Purpose**: The `dynamic` block is used to dynamically construct nested blocks within a resource. The configuration inside the `dynamic` block will repeat based on the items in the given collection, in this case, `local.sg_rules`.

### Iterator: `for_each`
- **Directive**: `for_each`
- **Value**: `local.sg_rules`
- **Purpose**: This iterates over each item in `local.sg_rules`, which is expected to be a map or a set of complex structures. Each iteration produces an instance of the `security_rule` block. The `for_each` map or set provides a context in which each item is referred to as `security_rule` inside the dynamic block (this is automatically done by Terraform).

### Content Block
- **Keyword**: `content`
- **Purpose**: Defines the content of the `security_rule` blocks that are dynamically generated for each element in the `local.sg_rules`. This is where you set the properties of each `security_rule` based on the attributes of each item in the provided map.

### Attributes within the Content Block:
- `name`: Set dynamically for each rule using the map key (`security_rule.key`).
- `priority`: Derived from the corresponding value in the `local.sg_rules` map (`security_rule.value.priority`).
- `direction`: Set from `security_rule.value.direction`, indicating whether the rule is for inbound or outbound traffic.
- `access`: Specifies whether the access is allowed or denied (`security_rule.value.access`).
- `protocol`: The protocol to which the rule applies, such as TCP, UDP, or ICMP (`security_rule.value.protocol`).
- `source_port_range`: Hardcoded to `"*"`, meaning it applies to all source ports.
- `destination_port_range`: Derived from `security_rule.value.destination_port_range`, specifying the destination ports affected by the rule.
- `source_address_prefix`: Hardcoded to `"*"`, indicating it applies to all source addresses.
- `destination_address_prefixes`: Specifies the destination subnets to which the rule applies, using `azurerm_subnet.lab-private.address_prefixes`, which dynamically sets the destination based on the address prefixes of a given subnet.

The `dynamic "security_rule"` block allows for the flexible and concise definition of multiple security rules within a network security group or similar resource, with each rule customized via a data structure (`local.sg_rules`). This approach can simplify Terraform configurations where multiple similar blocks are required, reducing redundancy and potential errors in large-scale configurations. This is an example of Terraform's capabilities to manage infrastructure declaratively while maintaining the dynamic adaptability needed for complex deployments.

</details>

When you are done, run terraform validate:

```shell
terraform validate
```

Run terraform plan.  If you have refactored the code correctly, the plan should come back with no changes to make.

```shell
terraform plan
```

![Terraform Plan - change sg to for_each](./images/tf-plan-sgrule.png "Terraform Plan - change sg to for_each")

---

### Summary

#### Lab Objective:
The main goal of this lab is to familiarize participants with the use of Terraform's dynamic blocks to manage multiple security group rules efficiently, replacing static blocks with more flexible and scalable configurations.

#### Preparation:
Participants should start from the setup completed in lab 4.4, ensuring that they have the necessary base configurations and outputs to integrate into the dynamic security group setups in this lab.

#### Lab Steps:

### Step 1: Update Outputs in tf-base Project
- **Output Addition**: Participants add outputs for the private subnet ID and the public subnet's address prefix in the `tf-base` project. These outputs facilitate data sharing between Terraform projects, which is essential for configuring network-related resources accurately in the `tf-project`.

### Step 2: Define Local Variables
- **Locals Setup**: In the `tf-project`, participants define new local variables to store subnet details fetched from the `tf-base` outputs. This step emphasizes the importance of managing cross-project dependencies and data flow.

### Step 3: Implement Dynamic Block for Security Group Rules
- **Security Group Refactoring**:
  - Participants refactor the existing multiple static `security_rule` blocks into a single dynamic block within the network security group resource. This is done by defining a map of security rule configurations in a `locals` block.
- **Dynamic Block Configuration**:
  - A `dynamic` block is added to the network security group resource, which iterates over each entry in the `locals` map (`local.sg_rules`). The `content` of the dynamic block uses these entries to set properties like priority, direction, access, protocol, and destination port range dynamically for each rule.

### Step 4: Configuration and Validation
- **Map Definition**:
  - Participants create a map in the `locals` block that contains configurations for each type of security rule, such as HTTP and SSH access. This map serves as the source for the `for_each` directive in the dynamic block.
- **Dynamic Block Implementation**:
  - The dynamic block is configured to dynamically generate `security_rule` sub-blocks based on the defined map. This setup allows easy scalability and modification of the rules without multiple separate resource definitions.
- **Validation**:
  - `terraform validate` is run to ensure there are no syntax or configuration errors in the updated Terraform files.

### Step 5: Plan and Apply
- **Plan Execution**:
  - `terraform plan` is executed to confirm that the refactoring does not introduce any unintended changes. Ideally, this should show no changes if the dynamic block correctly replicates the functionality of the previous static blocks.
- **Apply (if necessary)**:
  - If there are changes suggested by the plan (which should not be the case), `terraform apply` would be run to implement them. This step is more about verifying that the infrastructure remains stable and unchanged despite the refactoring.

### Educational Outcomes:
- **Dynamic Terraform Features**: Participants learn how to use dynamic blocks, enhancing their ability to write more adaptable and maintainable Terraform configurations.
- **Complex Configuration Management**: The lab teaches how to manage complex configurations more simply by reducing the repetition of similar blocks and using data structures to define necessary variations.
- **Cross-project Integration**: By fetching and utilizing outputs from another Terraform project (`tf-base`), participants gain experience in managing dependencies and data flow across different Terraform projects, a common scenario in larger or more complex infrastructure setups.

#### Conclusion:
This lab demonstrates the practical application of dynamic blocks in Terraform, showing how they can streamline the management of security rules within a network security group. Participants enhance their skills in using advanced Terraform features to maintain scalable and easily adjustable infrastructure configurations. This knowledge is helpful for efficient infrastructure as code practices in environments where requirements can change frequently.
