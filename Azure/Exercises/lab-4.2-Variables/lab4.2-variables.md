# Variables

Lab Objective:
- Add input variables to your configuration

# Preparation

If you did not complete lab 3.5, you can simply copy the solution code from that lab (and do terraform apply) as the starting point for this lab.

# Lab

First, think about what you might want to parameterize in the configuration you have defined so far.

:question: What variations might you want to support?  Should there be a default value for some parameters, and which should be required?

Create a file called variables.tf

For this lab, we will create variables for the following:
-	Region
- VM password
-	Database storage amount (default value is 5120)

Try your hand at writing the variable declarations in `variables.tf`.  Run terraform validate to check for syntax errors.

Compare your code to the solution below (or to the variables.tf file in the solution folder).

<details>

 _<summary>Click to see solution for variables</summary>_

```
variable "region" {
  type = string
}

variable "vm_password" {
  description = "6-20 characters. At least 1 lower, 1 cap, 1 number, 1 special char."
  type = string
  sensitive = true
}

variable "db_storage" {
  type = number
  default = 5120
}
```
</details>

Now, use a variable reference to replace the corresponding target expressions in the configuration files.  There should be three places:

- Set the region local value in `local.tf` with `var.region`
- Set the admin_password value in `vm.tf` with `var.vm_password`
- Set the storage_mb value in `database.tf` with `var.db_storage`

Run terraform validate to check for errors.

### Setting the Variable Values

Create a file called `terraform.tfvars`

Set the values for the variables in that file.  Keep the region the same as before to avoid recreating the entire infrastructure.  Keeping the password the same as before will avoid re-creating the virtual machine.  The database storage value must be a multiple of 1024 and greater than 5120.

```
region = "eastus2"
db_storage = 6144
vm_password = "<PASSWORD>"
```

:bangbang: Be sure to replace &lt;PASSWORD&gt; with the actual password string above. (If you forgot the VM password, you can look in the solution folder of a prior lab.)

> Storing passwords in a file is a strongly discouraged practice.  The virtual machine really should be using an SSH key for access instead of a password.  Including a password in the variables file is only for the convenience of this lab and should not be done in actual practice.

Run terraform plan:

```shell
terraform plan
```

:information_source: **Changing the database storage can be updated in place. If you changed the VM password, the virtual machine will need to be re-created.**

![Terraform Plan - after variable addition](./images/tf-plan-vars.png "Terraform Plan - after variable addition")

Run terraform apply:

```shell
terraform apply
```

### Extra Credit -- Validation

If you still have time, add a validation block for the `db_storage` variable in `variables.tf` to verify the db_storage variable is greater than or equal to 5120 and is a multiple of 1024.

```
variable "db_storage" {
  type = number
  default = 5120

  validation {
    condition = var.db_storage >= 5120 && var.db_storage % 1024 == 0
    error_message = "Minimum db storage is 5120 and must be multiple of 1024."
  }
}
```

Change the value of `db_storage` in `terraform.tfvars` file to 6140.  Run `terraform plan`.  You should get an error.  Change the value back to a valid value of 6144.

---

### Summary

#### Lab Objective:
The main objective of this lab is to introduce and implement input variables in a Terraform configuration, enhancing the flexibility and reusability of the code by allowing parameters to be passed externally.

#### Preparation:
Participants should start with the setup from lab 3.5 or use the provided solution code as a base for this lab.

#### Lab Steps:

### Step 1: Identify Variable Candidates
- Participants are encouraged to think about aspects of their configurations that could benefit from parameterization, such as those values that might vary between deployments (e.g., region, passwords, resource sizing).

### Step 2: Define Variables
- **File Creation**: Participants create a file named `variables.tf` to define all variable declarations.
- **Variable Definitions**:
  - **Region**: Defined to allow dynamic specification of the Azure region for resource deployment.
  - **VM Password**: Secured configuration for the virtual machine's administrative password.
  - **Database Storage**: Specifies the storage capacity for the database, with a default set to 5120 MB but allowing overrides.

### Step 3: Replace Configurations with Variables
- Participants update existing configurations to reference the newly defined variables, thereby decoupling specific values from the code and increasing modularity.

### Step 4: Validate Configuration
- Running `terraform validate` ensures that the variable integration does not contain any syntax errors.

### Step 5: Setting Variable Values
- **File Creation**: Participants create a `terraform.tfvars` file to set the values for the defined variables.
- **Variable Values**: Includes setting specific, realistic values for region, VM password, and database storage, aligning with previous setups to prevent unnecessary resource changes.

### Step 6: Apply Configuration
- Participants run `terraform plan` to review changes and then `terraform apply` to implement the new variable-driven configuration.

### Extra Credit: Add Validation
- Participants enhance their variable definitions with validation rules, specifically ensuring that the database storage size is appropriate for Azure's requirements (minimum size and multiple of 1024).

#### Educational Outcomes:
- **Enhanced Flexibility**: Participants learn how to make Terraform configurations more dynamic and flexible using variables, which can be adjusted per deployment without altering the main configuration files.
- **Security Practices**: The lab emphasizes secure practices by handling sensitive data like passwords carefully, though it notes the best practice of using SSH keys over passwords for production environments.
- **Error Handling**: Through validation, participants experience firsthand how to prevent common configuration errors and enforce policy constraints within their configurations.

#### Conclusion:
This lab effectively demonstrates how to use variables to make Terraform configurations more adaptable and maintainable. Participants gain hands-on experience with defining, applying, and validating variables, equipping them with skills necessary for managing complex, dynamic infrastructure setups in professional environments.