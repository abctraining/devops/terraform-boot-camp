# Outputs

Lab Objective:
- Add outputs for resource attributes

## Preparation

If you did not complete lab 4.2, you can simply copy the solution code from that lab (and do terraform apply) as the starting point for this lab.

## Lab

Look through the configuration files, and think about what outputs you might want to provide to users who instantiate this configuration.

:question: What attributes have you needed to find so you can use them in running commands.

For this lab, we will output two values:
- Public IP of virtual machine (to include in SSH command)
- Database endpoint (to include in a psql command to connect to the database)

Create a file called `outputs.tf`

Add outputs for the above two attributes to the file.  Try your hand at it first before looking at the solution.  You might want to take a look at the Terraform documentation to see what attributes are exported for the two resources:
- azurerm_linux_virtual_machine
- azurerm_postgresql_server

<details>

 _<summary>Click to see solution for outputs</summary>_

These blocks define two output values for a Terraform configuration. Outputs in Terraform are used to extract information about the resources created by Terraform, which can be useful for displaying on the command line, using outside of Terraform, or feeding into other Terraform configurations.

```
output "bastion-vm-public-ip" {
  value = azurerm_linux_virtual_machine.lab-bastion.public_ip_address
}

output "db-server-endpoint" {
  value = azurerm_postgresql_server.lab.fqdn
}
```

### Output Block 1: `bastion-vm-public-ip`
- **Purpose**: This output is used to display or retrieve the public IP address assigned to a virtual machine, specifically one named as `lab-bastion`. This VM appears to be configured as a bastion host, which is typically used as a secure, controlled entry point for accessing other resources in a private network.
- **Configuration**:
  - `value`: Specifies the value that the output will return. Here, it refers to the `public_ip_address` attribute of the resource `azurerm_linux_virtual_machine` identified by the name `lab-bastion`. This attribute contains the public IP address assigned to the VM.

### Output Block 2: `db-server-endpoint`
- **Purpose**: This output is used to display or retrieve the fully qualified domain name (FQDN) of a PostgreSQL server, identified here as `lab`. This information is often necessary for applications and services that need to connect to the database server.
- **Configuration**:
  - `value`: Specifies the value that the output will return. It extracts the `fqdn` attribute from the `azurerm_postgresql_server` resource named `lab`. The FQDN is essential for network connections to the database server over the internet or within cloud services.

</details>

### General Usage:
- **`output` Keyword**: Indicates that the block is defining an output value, which Terraform will display at the end of the `apply` operation or can be queried using the `terraform output` command.
- **Accessibility**: These outputs make important connectivity details easily accessible, aiding in system integration, troubleshooting, and documentation processes.

These output definitions enhance the usability and accessibility of key infrastructure components by making critical connection details readily available after deployment. This is particularly useful in automated environments, where scripts or other automation tools might need to fetch these values programmatically after infrastructure provisioning is completed.

Run terraform validate to check for syntax errors:

```shell
terraform validate
```

Run terraform plan. See that the execution plan will be adding the outputs to the state. Since the output values are derived from existing state, the plan will also show you the values.  Are they what you expect?

```shell
terraform plan
```

![Terraform Plan - Outputs](./images/tf-plan-outputs.png "Terraform Plan - Outputs")

Run terraform apply.  This is necessary to save the output values into the Terraform state.

```shell
terraform apply
```

The output values will show up in the console at the end of the apply console output.

![Terraform Apply - Outputs](./images/tf-apply-outputs.png "Terraform Apply - Outputs")

You can use terraform output to view the output values now that they are part of the Terraform state.

```shell
terraform output bastion-vm-public-ip
```

```shell
terraform output db-server-endpoint
```

![Terraform Output](./images/tf-output.png "Terraform Output")

---

### Summary

#### Lab Objective:
The main objective of this lab is to teach participants how to define and use output values in Terraform configurations. This enhances the ability to extract and display important information about the infrastructure that Terraform manages.

#### Preparation:
Participants should use the configurations from lab 4.2 as a starting point to ensure they have a consistent setup for this lab.

#### Lab Activities:

### Step 1: Identify Outputs
- Participants are encouraged to review their existing Terraform configurations to identify potential outputs. Specifically, they should consider which resource attributes they frequently need to access, such as public IP addresses or database endpoints.

### Step 2: Create and Define Outputs
- **File Creation**: Participants are instructed to create a file named `outputs.tf` to organize output definitions.
- **Output Definitions**:
  - **Public IP of the VM**: This output helps users retrieve the public IP address of a virtual machine configured as a bastion host, which is essential for accessing the VM, especially for SSH commands.
  - **Database Endpoint**: This output provides the fully qualified domain name (FQDN) of a PostgreSQL server, necessary for database connection strings used in applications or during database management tasks.

### Step 3: Implement and Validate Outputs
- Participants write the output declarations using the attributes of the `azurerm_linux_virtual_machine` and `azurerm_postgresql_server` resources.
- Running `terraform validate` checks for syntax errors in the output definitions.

### Step 4: Apply Configuration
- **Terraform Plan**: Running `terraform plan` allows participants to preview the addition of outputs to their Terraform state, confirming that no resources will be recreated or modified unexpectedly.
- **Terraform Apply**: Applying the changes updates the Terraform state with the new output values, making them readily accessible post-deployment.

### Step 5: Retrieve Outputs
- After applying, participants use the `terraform output` command to retrieve and verify the output values, ensuring they are correct and accessible.

### Educational Outcomes:
- **Practical Usage of Outputs**: Participants learn how to effectively use outputs to extract critical information from Terraform-managed resources, which can be crucial for integration with other systems or for operational documentation.
- **Enhanced Terraform Skills**: The lab enhances participants' understanding of Terraform's capabilities, particularly in managing and organizing infrastructure information which can be leveraged in real-world scenarios to improve infrastructure visibility and accessibility.
- **Debugging and Validation**: Participants gain experience in validating and debugging Terraform configurations, an essential skill for maintaining reliable infrastructure as code practices.

#### Conclusion:
This lab provides a comprehensive approach to using Terraform outputs, from defining what information should be outputted to applying and retrieving these outputs. By the end of the lab, participants will be equipped with the knowledge to enhance their Terraform projects with useful outputs, improving both the usability and manageability of their infrastructure setups.
