locals {
  resource_group = "aztf-lab-rg"
  subnet_public = "/subscriptions/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/resourceGroups/aztf-lab-rg/providers/Microsoft.Network/virtualNetworks/aztf-labs-vnet/subnets/aztf-labs-subnet-public"
  subnet_private = "/subscriptions/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/resourceGroups/aztf-lab-rg/providers/Microsoft.Network/virtualNetworks/aztf-labs-vnet/subnets/aztf-labs-subnet-private"
  subnet_public_address_prefix = "10.0.0.0/24"
  subnet_private_address_prefix = "10.0.1.0/24"
  region = var.region
  common_tags = merge(var.tags,{
    Environment = "Lab"
    Project     = "AZTF Training"
  })
  size_spec = {
    low = {
      cluster_size = 1
    },
    medium = {
      cluster_size = 2
    },
    high = {
      cluster_size = 3
    }
  }
  cluster_size = try(coalesce(var.node_count, lookup(local.size_spec,var.load_level).cluster_size), 1)
}
