locals {
  resource_group = "aztf-lab-rg"
  subnet_public = "/subscriptions/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/resourceGroups/aztf-lab-rg/providers/Microsoft.Network/virtualNetworks/aztf-labs-vnet/subnets/aztf-labs-subnet-public"
  region = "eastus2"
  common_tags = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}
