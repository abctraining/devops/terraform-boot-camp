terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
  }
  backend "azurerm" {
    resource_group_name  = "aztf-backend-rg"
    container_name       = "tfstate"
    key                  = "base.terraform.tfstate"
  }
  required_version = ">= 1.6.4"
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "base" {
  name     = "aztf-lab-rg"
  location = "eastus2"
  tags     = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}

resource "azurerm_virtual_network" "lab" {
  name                = "aztf-labs-vnet"
  location            = "eastus2"
  resource_group_name = azurerm_resource_group.base.name
  address_space       = ["10.0.0.0/16"]
  tags                = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}

resource "azurerm_subnet" "lab-public" {
  name                 = "aztf-labs-subnet-public"
  resource_group_name  = azurerm_resource_group.base.name
  virtual_network_name = azurerm_virtual_network.lab.name
  address_prefixes     = ["10.0.0.0/24"]
}

resource "azurerm_subnet" "lab-private" {
  name                 = "aztf-labs-subnet-private"
  resource_group_name  = azurerm_resource_group.base.name
  virtual_network_name = azurerm_virtual_network.lab.name
  address_prefixes     = ["10.0.1.0/24"]
}

resource "azurerm_network_security_group" "lab-public" {
  name                = "aztf-labs-public-sg"
  location            = "eastus2"
  resource_group_name = azurerm_resource_group.base.name
}

resource "azurerm_subnet_network_security_group_association" "lab-public" {
  subnet_id                 = azurerm_subnet.lab-public.id
  network_security_group_id = azurerm_network_security_group.lab-public.id
}

output "subnet-public" {
  value = azurerm_subnet.lab-public.id
}
