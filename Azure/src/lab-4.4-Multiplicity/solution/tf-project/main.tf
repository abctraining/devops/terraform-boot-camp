terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6"
    }
    azuread = {
      source = "hashicorp/azuread"
      version = "~> 2.47"
    }
  }
  backend "azurerm" {
    resource_group_name  = "aztf-backend-rg"
    container_name       = "tfstate"
    key                  = "project.terraform.tfstate"
  }
  required_version = ">= 1.6.4"
}

provider "azurerm" {
  features {}
}

provider "random" {
}

provider "azuread" {
  use_msi = false
}
