locals {
  resource_group = "aztf-lab-rg"
  subnet_public = "/subscriptions/XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX/resourceGroups/aztf-lab-rg/providers/Microsoft.Network/virtualNetworks/aztf-labs-vnet/subnets/aztf-labs-subnet-public"
  subnet_private_address_prefix = "10.0.1.0/24"
  region = var.region
  common_tags = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}
