terraform {
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
  }
  required_version = ">= 1.6.4"
}

provider "random" {
}

provider "azurerm" {
  features {}
}

resource "random_integer" "number" {
  min     = 1000
  max     = 9999
}

resource "azurerm_resource_group" "backend" {
  name     = "aztf-backend-rg"
  location = "eastus2"
  tags     = {
    Environment = "Lab"
    Project     = "AZTF Backend"
  }
}

resource "azurerm_storage_account" "backend" {
  name                     = "abcaztfbackend${random_integer.number.result}"
  resource_group_name      = "aztf-backend-rg"
  location                 = "eastus2"
  account_tier             = "Standard"
  account_replication_type = "GRS"
}

resource "azurerm_storage_container" "backend" {
  name                  = "tfstate"
  storage_account_name  = "abcaztfbackend${random_integer.number.result}"
  container_access_type = "private"
}

output "storage_account_name" {
  value = azurerm_storage_account.backend.name
}
