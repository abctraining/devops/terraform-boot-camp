terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
  }
  backend "azurerm" {
    resource_group_name  = "aztf-backend-rg"
    container_name       = "tfstate"
    key                  = "project.terraform.tfstate"
  }
  required_version = ">= 1.6.4"
}

provider "azurerm" {
  features {}
}
