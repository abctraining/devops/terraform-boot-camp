locals {
  sg_rules = {
    HTTP-Access = {
      priority               = 100,
      direction              = "Inbound",
      access                 = "Allow",
      protocol               = "Tcp",
      destination_port_range = 80
    },
    SSH-Access = {
      priority               = 110,
      direction              = "Inbound",
      access                 = "Allow",
      protocol               = "Tcp",
      destination_port_range = 22
    }
  }
}  

resource "azurerm_network_security_group" "lab-private" {
  name                = "aztf-labs-private-sg"
  location            = "eastus2"
  resource_group_name = azurerm_resource_group.base.name

  dynamic "security_rule" {
    for_each = local.sg_rules
    content {
      name                         = security_rule.key
      priority                     = security_rule.value.priority
      direction                    = security_rule.value.direction
      access                       = security_rule.value.access
      protocol                     = security_rule.value.protocol
      source_port_range            = "*"
      destination_port_range       = security_rule.value.destination_port_range
      source_address_prefix        = "*"
      destination_address_prefixes = azurerm_subnet.lab-private.address_prefixes
    }
  }
}

resource "azurerm_subnet_network_security_group_association" "lab-private" {
  subnet_id                 = azurerm_subnet.lab-private.id
  network_security_group_id = azurerm_network_security_group.lab-private.id
}
