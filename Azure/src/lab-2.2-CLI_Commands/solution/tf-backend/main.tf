terraform {
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6"
    }
  }
  required_version = ">= 1.6.4"
}

provider "random" {
}

resource "random_integer" "number" {
  min     = 1000
  max     = 9999
}
