terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
  }
  backend "azurerm" {
    resource_group_name  = "aztf-backend-rg"
    container_name       = "tfstate"
    key                  = "base.terraform.tfstate"
  }
  required_version = ">= 1.6.4"
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "base" {
  name     = "aztf-lab-rg"
  location = "eastus2"
  tags     = {
    Environment = "Lab"
    Project     = "AZTF Training"
  }
}
