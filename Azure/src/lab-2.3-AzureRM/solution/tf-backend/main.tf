terraform {
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "~> 3.6"
    }
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.96"
    }
  }
  required_version = ">= 1.6.4"
}

provider "random" {
}

provider "azurerm" {
  features {}
}

resource "random_integer" "number" {
  min     = 1000
  max     = 9999
}

resource "azurerm_resource_group" "backend" {
  name     = "aztf-backend-rg"
  location = "eastus2"
  tags     = {
    Environment = "Lab"
    Project     = "AZTF Backend"
  }
}
