# Outputs
##########

output "vm_password" {
  description = "The randomly generated password for the VM."
  value       = random_password.example.result
  sensitive   = true
}

output "vm_public_ip" {
  description = "The public IP address of the Virtual Machine."
  value       = azurerm_public_ip.example.ip_address
}
