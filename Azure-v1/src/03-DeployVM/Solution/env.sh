#!/bin/bash

# Create the .ssh directory for key store in the clouddrive
mkdir -p ~/clouddrive/.ssh
chmod 700 ~/clouddrive/.ssh
cd ~/clouddrive/.ssh

# Generate a new key
ssh-keygen -t rsa -b 4096 -f id_rsa -N ""

# Restrict access to the new key
chown 700 ~/clouddrive/.ssh/id_rsa
