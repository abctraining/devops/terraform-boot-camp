# Azure Services
# Hands-on Exercises #0

### Objective

In these exercises we will connect and initialize the lab environment

### Parts

[Part 1: Connect to Azure](Part-01-ConnectToAzure.md)

[Part 2: Cloud Shell](Part-02-CloudShell.md)

Return to the course [Table of Content](../README.md)
