# Exercise #0 : Part 2
## Cloud Shell

---

### Intro


---

At the conclusion of the last part of this exercise we where presented with a "Your deployment is complete" message.  Now we would like to connect to a shell that allows us to have a place to run Terraform from.  We will use the Azure "Cloud Shell" for that purpose.

### Cloud Shell

Cloud Shell has two options "Bash" or "PowerShell".  We will be using "Bash" so Select the "Bash" link.

![Cloud Shell - Bash](images/00-03-03.png "Cloud Shell - Bash")

The first time you start your "Cloud Shell" you will need to create a storage share to provide a location to store your shell files.  Click on the "Create storage" button.

![Cloud Shell - Create storage](images/00-03-04.png "Cloud Shell - Create storage")

After a short time you will be presented with a shell.  Notice that the two require "az" commands have already been executed for this session.

![Cloud Shell - Connected](images/00-03-05.png "Cloud Shell - Connected")

---

The azure shell we are using is running on a container that has the "az" configuration setup for our account.  _Note: If you want to run this cloud shell locally as a container you can by using the 'mcr.microsoft.com/azure-cloudshell:latest' image.  More info can be found [here](https://github.com/Azure/CloudShell)._

Verify that the 'az' is tied to the azure account.

```bash
az account list
```

![Cloud Shell - az account](images/00-03-06.png "Cloud Shell - az account")

### A scavenger hunt

Now it's time for a scavenger hunt.  The `src` files located within the Azure section of the Git repo found in GitLab.  See if you can locate the file associated with Hands-on Exercise 00-Azure - Cloud Shell.  Once you locate that file you can view the content by concatenating it with the `cat` command.

### Conclusion

You now have the access and tools needed to complete upcoming hands-on exercises.  Over the next days we will get quite a bit of hands on experience with Terraform.

---

This concludes part 2 of Hands-on Exercise #0.

Return to the [Exercise Page](../README.md)