# Exercise #03 : Part 1
## Deploying Azure Virtual Machines

---

### Welcome to Section 3 of the Terraform Boot Camp Workshop!

Welcome back, eager learners! In this section, we will explore the exciting world of deploying Azure Virtual Machines (VMs) using Terraform. VMs are fundamental building blocks for many applications and services in the cloud. By the end of this section, you will be equipped with the knowledge and skills to create and manage Azure VMs with ease. Let's dive right in!

### Objective 1: Creating Azure Virtual Machines using Terraform

Azure VMs can be created using the `azurerm_virtual_machine` resource block in your Terraform configuration. Here's a basic example:

```hcl
resource "azurerm_virtual_machine" "example" {
  name                  = "example-vm"
  location              = "East US"
  resource_group_name   = azurerm_resource_group.example.name
  network_interface_ids = [azurerm_network_interface.example.id]
  vm_size               = "Standard_B1s"
}
```

This code defines an Azure VM named "example-vm" in the "East US" region, within a specified resource group. 

### Objective 2: Configuring Virtual Machine properties and size

You can configure various properties of your VMs in the Terraform configuration, such as the VM size, operating system, and more. For example, to set the VM size to `Standard_B1ls2`, you can modify the `vm_size` attribute in the `azurerm_virtual_machine` resource block.

```hcl
resource "azurerm_virtual_machine" "example" {
  name                  = "example-vm"
  location              = "East US"
  resource_group_name   = azurerm_resource_group.example.name
  network_interface_ids = [azurerm_network_interface.example.id]
  vm_size               = "Standard_B1ls2"
}
```

### Objective 3: Managing Authentication and Credentials

When creating VMs, you need to specify how you want to authenticate into them. You can use SSH keys for Linux VMs and passwords for Windows VMs. Here's an example of setting a password for a Windows VM:

```hcl
resource "azurerm_virtual_machine" "example" {
  # ...
  os_profile {
    computer_name  = "example-vm"
    admin_username = "adminuser"
    admin_password = "SuperSecretPassword1"
  }
  # ...
}
```

### Objective 4: Accessing the Virtual Machine

After creating a VM, you can access it using remote desktop (RDP) for Windows VMs or SSH for Linux VMs. Ensure you have the necessary credentials to access the VM as configured in the Terraform script.

### Challenges for Section 3

1. **Challenge 1**: Write a Terraform configuration to create an Azure Virtual Machine named "my-vm" with the size `Standard_B1s` in your "example" project.
2. **Challenge 2**: Configure the VM to use an SSH key for authentication.
3. **Challenge 3**: Create a Windows VM in your "example" project with a custom admin username and password.
4. **Challenge 4**: Access the VMs you created using either SSH or RDP.

---

Are you ready to take on these challenges? By the end of this section, you'll be proficient in creating, configuring, and accessing Azure Virtual Machines using Terraform. Practice makes perfect, so don't hesitate to try out different configurations! You've got this! 🚀

---


- Terraform AzureRM Provider
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs

- Terraform Random Provider
https://registry.terraform.io/providers/hashicorp/random/latest/docs

- Terraform Resource: random_password
https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password

- Terraform resource: azurerm_virtual_machine
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_machine

- Terraform resource: azurerm_network_interface
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/network_interface

- Terraform resource: azurerm_public_ip
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/public_ip

- Terraform State
https://developer.hashicorp.com/terraform/language/state

- Terraform Outputs
https://developer.hashicorp.com/terraform/language/values/outputs

---
Work on completing the challenges listed above.  Try to complete them on your own without using the solution.  Once completed or if you get stuck continue on to the "Solution".

[Part 2: Solution](Part-02-Solution.md)
