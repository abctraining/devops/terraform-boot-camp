# Exercise #03 : Part 2
## Deploying Azure Virtual Machines

---

#### Preparation: Generating SSH Keys in the Cloud Shell

Before starting the challenges, generate SSH keys to use for authentication:

1. In the Azure Cloud Shell, create a new directory to store SSH keys:

```bash
mkdir ~/clouddrive/.ssh
chmod 700 ~/clouddrive/.ssh
```

2. Navigate to the new directory:

```bash
cd ~/clouddrive/.ssh
```

3. Generate a new SSH key pair:

```bash
ssh-keygen -t rsa -b 4096 -f id_rsa
```

Press enter to accept the default file location and to set no passphrase.

4. Restrict access to the Private Key:

```shell
chown 700 ~/clouddrive/.ssh/id_rsa
```

#### Challenge 1: Create an Azure Virtual Machine named "my-vm"

1. Open the `main.tf` file in your "labs" project directory:

```bash
cd ~/clouddrive/labs
nano main.tf
```

2. Add the following code to create a Linux VM:

```hcl
resource "azurerm_virtual_machine" "my_vm" {
  name                  = "my-vm"
  location              = azurerm_resource_group.example.location
  resource_group_name   = azurerm_resource_group.example.name
  network_interface_ids = [azurerm_network_interface.example.id]
  vm_size               = "Standard_B1s"
}
```

#### Challenge 2: Configure the VM to use an SSH Key for Authentication

1. Expand the "my-vm" resource block in `main.tf`:

```hcl
resource "azurerm_virtual_machine" "my_vm" {
  # ...

  os_profile {
    computer_name  = "myvm"
    admin_username = "azureuser"
  }

  storage_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  storage_os_disk {
    name              = "myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"

  }

  os_profile_linux_config {
    disable_password_authentication = false

    ssh_keys {
      path     = "/home/azureuser/.ssh/authorized_keys"
      key_data = file("~/clouddrive/.ssh/id_rsa.pub")
    }
  }
}
```

This `os_profile` section configures the VM to use the generated SSH key for authentication.

#### Challenge 3: Generate and Assign a Random Password to the VM

1. Add a `random_password` resource to generate a random password:

```hcl
resource "random_password" "example" {
  length  = 16
  special = true
}
```

2. Modify the `os_profile` section in the VM resource to use the generated password:

```hcl
os_profile {
  computer_name = "myvm"  
  admin_username = "azureuser"
  admin_password = random_password.example.result
}
```

3. Add an `output` for the `random_password` resource :

```hcl
output "myvm-password" {
  value     = random_password.example.result
  sensitive = true
}
```

#### Additional Configuration: Create `azurerm_network_interface`

1. Add the necessary configuration to create a network interface connected to the virtual network:

```hcl
resource "azurerm_network_interface" "example" {
  name                = "example-nic"
  location            = azurerm_resource_group.example.location
  resource_group_name = azurerm_resource_group.example.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.example.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.example.id
  }
}

resource "azurerm_public_ip" "example" {
  name = "myvmPublicIP"
  resource_group_name = azurerm_resource_group.example.name
  location = azurerm_resource_group.example.location
  allocation_method = "Dynamic"
}
```

#### Adding Outputs: Display the Public IP of the VM

1. Define an output variable to display the public IP of the VM:

```hcl
output "vm_public_ip" {
  value = azurerm_public_ip.example.ip_address
}
```

#### Applying the Configuration

1. In the "labs" directory, initialize and apply the configuration:

```bash
terraform init
terraform apply
```

2. Type `yes` when prompted to apply the changes.

3. The (sensitive value) for the password can be retrieved:

```bash
terraform output --raw myvm-password
```

4. You know have enough information to make a connection to the VM:

```shell
ssh azureuser@<VM-PUBLIC-IP>
```

---

Congratulations! You’ve completed Section 3 challenges. You've learned how to create an Azure VM, configure SSH authentication, generate a random password, and output the public IP of your VM using Terraform! 🌟 Keep experimenting and practicing!

---

This concludes part 2 of Hands-on Exercise #03.

Return to the [Exercise Page](../README.md)