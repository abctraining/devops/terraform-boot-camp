# Exercise #08 : Part 2
## Final Project and Review

---

### Final Hands-On Challenges

#### Challenge 1: Comprehensive Infrastructure with Conditionals and Loops
- Create a configuration that provisions multiple VMs conditionally.
- Use loops to create a dynamic number of identical resources, like VMs or databases.
- Implement conditional expressions to tailor resource properties based on environment variables.

#### Challenge 2: State Management in a Multi-Environment Setup
- Configure backends for different environments.
- Secure sensitive data in state files.
- Examine and interpret the state files of different environments.

#### Challenge 3: Modular Multi-Tier Application
- Structure a multi-tier application using modules.
- Ensure modules are reusable and versioned.
- Utilize outputs from one module as inputs for another.

#### Challenge 4: Scalable and Conditional VM Deployment
- Deploy a scalable set of VMs using `count` or `for_each`.
- Conditionally create resources, and use outputs based on the condition.
- Secure sensitive information like admin passwords.

#### Challenge 5: Advanced Networking with Modules
- Implement a network topology using modules, including VNETs, subnets, and security groups.
- Ensure resources are associated correctly, utilizing outputs from network modules in compute modules.

#### Challenge 6: Conditional Environment Deployment
- Develop a configuration to deploy distinct resources based on the specified environment (production, staging, or development).
- Utilize conditional logic to determine the number and type of resources deployed.

#### Challenge 7: Secure and Scalable Database Deployment
- Implement a secure and scalable database solution.
- Use random providers for password generation.
- Implement secure backend storage for state files, protecting sensitive data.

#### Challenge 8: Dynamic Resource Naming and Tagging
- Ensure that resources are named and tagged according to the environment and purpose.
- Implement randomness and conditionals for resource naming conventions.

#### Challenge 9: Integrated Multi-Service Architecture
- Deploy a combination of different services (VMs, databases, networks) into a cohesive architecture.
- Utilize modules and outputs to integrate and reference resources across the architecture.

#### Challenge 10: Advanced State Management and Manipulation
- Implement a backend configuration for advanced state management.
- Execute state manipulations such as state mv and rm to manage resources effectively.

Each challenge should encompass various aspects covered during the course, ensuring a comprehensive understanding and application of Terraform principles, including resource creation, modularization, conditionals, loops, state management, and security. Good luck, and may your configurations be ever successful! 🌐🚀

---

This concludes part 2 of Hands-on Exercise #08.

Return to the [Exercise Page](../README.md)