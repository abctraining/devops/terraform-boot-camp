# Exercise #07 : Part 2
## Conditional Expressions and Loops

---

### Section 7 Challenges Solutions

#### Challenge 1: Writing Conditional Expressions in Configurations

Let’s create a conditional expression that outputs whether the environment is production or not.

```hcl
variable "environment" {
  description = "Deployment environment"
  type        = string
}

output "environment_type" {
  value = var.environment == "production" ? "Production Environment" : "Non-Production Environment"
}
```

Set the variable "environment" and then apply your configuration to see the output.

#### Challenge 2: Creating Resources Based on Conditional Logic

Let’s conditionally create an Azure public IP based on a boolean variable.

```hcl
variable "create_public_ip" {
  description = "Flag to create Public IP"
  type        = bool
  default     = false
}

resource "azurerm_public_ip" "conditionalIP" {
  count               = var.create_public_ip ? 1 : 0
  name                = "conditionalPublicIP"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  allocation_method   = "Dynamic"
}
```

Set the variable "create_vm" and then initialize and apply your configuration to conditionally create the VM.  This is a good example of where testing with the `-env` flag and `terraform plan` come in handy.

**With Public IP:**
```bash
terraform plan -var 'create_public_ip=true'
```

**Without Public IP:**
```bash
terraform plan -var 'create_public_ip=false'
```



#### Challenge 3: Implementing `for_each` and `count` Loops

We'll create multiple resource groups using a loop.

```hcl
variable "rg_names" {
  description = "Names of the resource groups"
  type        = list(string)
}

resource "random_string" "unique_rg_id" {
  length  = 4
  special = false
}

resource "azurerm_resource_group" "loop" {
  for_each = toset(var.rg_names)
  name     = "${each.value}-${random_string.unique_rg.id}"
  location = azurerm_resource_group.example.location
}
```

Define the "rg_names" variable and apply the configuration to create multiple resource groups.

Add the list to `terraform.tfvars`:
```
rg_names = [ "one", "two", "three" ]
```

### Applying the Configuration

1. Plan the configuration to see the execution plan:

```bash
terraform plan
```

2. Apply the configuration:

```bash
terraform apply
```

2. List the resource groups:

```bash
az group list
```


#### Challenge 4: Managing and Modifying a Module Reference Using Loops

Suppose we have a module that creates networks and subnets. We’ll create multiple instances of this module.

```hcl
variable "vnets" {
  description = "A set of network for the vnet module"
  type        = map(any)
  default = {
    north = {
      network_cidr = "10.101.0.0/16"
      subnet_cidr  = "10.101.1.0/24"
    }
    south = {
      network_cidr = "10.102.0.0/16"
      subnet_cidr  = "10.102.1.0/24"
    }
    east = {
      network_cidr = "10.103.0.0/16"
      subnet_cidr  = "10.103.1.0/24"
    }
    west = {
      network_cidr = "10.104.0.0/16"
      subnet_cidr  = "10.104.1.0/24"
    }  
  }
}


module "vnet" {
  source                = "./network_module"
  for_each              = var.vnets

  resource_group        = azurerm_resource_group.example.name
  location              = azurerm_resource_group.example.location
  network_address_space = each.value.network_cidr
  subnet_address_space  = each.value.subnet_cidr
}
```

### Applying the Configuration

1. Initialize the Terraform configuration:

```bash
terraform init
```

2. Plan the configuration to see the execution plan:

```bash
terraform plan
```

3. Apply the configuration:

```bash
terraform apply
```

Confirm the changes, and Terraform will execute the actions proposed in your configuration.

---

Great job on overcoming the challenges of Section 7! You've mastered the dynamic constructs like conditional expressions and loops in Terraform, bringing adaptability and intelligence to your configurations. Keep exploring, and happy terraforming! 🚀🌐

---

This concludes part 2 of Hands-on Exercise #07.

Return to the [Exercise Page](../README.md)