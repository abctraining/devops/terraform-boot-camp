# Exercise #07 : Part 1
## Conditional Expressions and Loops

---

### Welcome to Section 7 of the Terraform Boot Camp Workshop!

Welcome once again, Cloud architects! 🚀 As we ascend further into the Terraform cosmos, we'll unravel the dynamic constructs that empower our configurations with flexibility and intelligence. In this section, we will dive into conditional expressions and loops, equipping you with the tools to craft responsive and scalable infrastructure as code!

### Objective 1: Using Conditional Expressions in Terraform

Conditional expressions allow your configurations to react dynamically to input variables or other runtime information.

**Example:**
```hcl
output "instance_type" {
  value = var.environment == "production" ? "Standard_B1s" : "Standard_B1ls"
}
```
In this example, the instance type output will be decided based on the value of the `environment` variable.

### Objective 2: Creating Resources Conditionally

You can control whether resources are created using conditional logic, providing flexibility in managing different environments or configurations.

**Example:**
```hcl
resource "azurerm_virtual_machine" "example" {
  count               = var.create_vm ? 1 : 0
  # ... other configurations ...
}
```
In this example, the virtual machine will be created if the `create_vm` variable is true.

### Objective 3: Implementing Looping Constructs in Terraform

Loops like `for_each` and `count` allow you to manage multiple instances of resources or modules effortlessly.

**Example using `count`:**
```hcl
resource "azurerm_virtual_machine" "example" {
  count               = length(var.vm_names)
  # ... other configurations ...
}
```

### Objective 4: Managing Multiple Resource Instances through Loops

Loops enable you to manage a dynamic number of resource instances, making your configurations more adaptable.

**Example using `for_each`:**
```hcl
resource "azurerm_virtual_machine" "example" {
  for_each            = toset(var.vm_names)
  # ... other configurations ...
}
```

### Challenges for Section 7

1. **Challenge 1**: Experiment with conditional expressions by applying them to variables or outputs in your "example" project.
2. **Challenge 2**: Create resources conditionally based on a boolean variable’s value in your configuration.
3. **Challenge 3**: Implement `for_each` and `count` loops to manage multiple resource instances dynamically.
4. **Challenge 4**: Adapt a module reference in your configuration to manage multiple module instances through loops.

---

Embark on these challenges with a spirit of exploration. Harness the power of conditionals and loops to unfold new possibilities in your Terraform adventures. Good luck, and happy terraforming! 🌌🔄

---

Helpful URLs for these challenges:

- Terraform AzureRM Provider
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs

- Terraform Random Providor
https://registry.terraform.io/providers/hashicorp/random/latest/docs

- Terraform Resource: random_password
https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password

- Terraform resource: azurerm_virtual_machine
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_machine

- Terraform State
https://developer.hashicorp.com/terraform/language/state

- Terraform Outputs
https://developer.hashicorp.com/terraform/language/values/outputs

- Terraform Variables
https://developer.hashicorp.com/terraform/language/values/variables

---

Helpful URLs for these challenges:

---
Work on completing the challenges listed above.  Try to complete them on your own without using the solution.  Once completed or if you get stuck continue on to the "Solution".

[Part 2: Solution](Part-02-Solution.md)
