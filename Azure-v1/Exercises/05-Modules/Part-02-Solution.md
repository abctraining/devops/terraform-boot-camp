# Exercise #05 : Part 2
## Modules and Reusability

---

### Section 5 Challenges Solutions

#### Challenge 1: Create a Module for Creating a Virtual Network and Subnet

1. Create a directory named `network_module` within the "labs" project directory.
2. Inside the `network_module` directory, create the following files:

**`variables.tf`**
```hcl
variable "resource_group" {
  description = "Resource group for the network resources."
  type        = string
}

variable "location" {
  description = "Azure location to deploy the network resources."
  type        = string
}

variable "network_address_space" {
  description = "Address space for the virtual network."
  type        = string
}

variable "subnet_address_space" {
  description = "Address space for the subnet."
  type        = string
}
```

**`main.tf`**
```hcl
resource "random_string" "unique_id" {
  length  = 8
  special = false
}

resource "azurerm_virtual_network" "example" {
  name                = "vnet-${random_string.unique_id.result}"
  address_space       = [var.network_address_space]
  location            = var.location
  resource_group_name = var.resource_group
}

resource "azurerm_subnet" "example" {
  name                 = "subnet-${random_string.unique_id.result}"
  resource_group_name  = var.resource_group
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = [var.subnet_address_space]
}
```

**`outputs.tf`**
```hcl
output "subnet_id" {
  value = azurerm_subnet.example.id
}
```

#### Challenge 2: Utilize the Created Module in the Main Configuration

Create a new  `network.tf` in your root module, call the created module:

```hcl
module "network" {
  source                = "./network_module"
  resource_group        = azurerm_resource_group.example.name
  location              = azurerm_resource_group.example.location
  network_address_space = "10.42.0.0/16"
  subnet_address_space  = "10.42.1.0/24"
}
```

Each module call will create a new virtual network with the specified configurations, demonstrating the reusability of the module.

### Applying the Configuration

1. Navigate to the "labs" directory in your terminal.
2. Initialize the configuration:

```bash
terraform init
```

3. Apply the configuration:

```bash
terraform apply
```

4. Confirm the changes by typing `yes` when prompted.

---

Congratulations! You've successfully navigated through the challenges of Section 5, mastering the creation, usage, and management of modules in Terraform. This knowledge is pivotal for writing scalable and maintainable Terraform configurations. Well done! 🌟🚀

---

This concludes part 2 of Hands-on Exercise #05.

Return to the [Exercise Page](../README.md)