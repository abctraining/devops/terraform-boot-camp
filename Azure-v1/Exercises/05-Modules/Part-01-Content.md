# Exercise #05 : Part 1
## TITLE

---

### Welcome to Section 5 of the Terraform Boot Camp Workshop!

Hello again, Cloud enthusiasts! 🚀 In this transformative section, we'll be unlocking the mysteries of Terraform Modules. Modules are a powerful feature that allow you to encapsulate and reuse code, making your configurations more manageable and scalable. Buckle up as we take a voyage through creating, using, and managing modules with precision and clarity!

### Objective 1: Introduction to Terraform Modules

Modules in Terraform are self-contained packages of Terraform configurations that are managed as a group. Modules are used for creating reusable components, and for organizing code to be manageable at scale.

**Example: Structure of a Module**

```plaintext
example-module/
│   main.tf
│   variables.tf
│   outputs.tf
```

### Objective 2: Creating and Using Modules

You can create your module by organizing related resources and configurations into a directory. Once your module is created, it can be utilized in your configurations.

**Example: Creating a Module**

`example-module/main.tf`

```hcl
resource "azurerm_virtual_machine" "example" {
  # ... (configuration details here)
}
```

**Example: Using a Module**

In your main configuration (`example/main.tf`), you can call the module like this:

```hcl
module "example_module" {
  source = "./example-module"
  # ... (pass any required variables here)
}
```

### Objective 3: Managing Module Sources and Versions

You can source modules from various locations such as the Terraform Registry, GitHub, Bitbucket, and others. Also, version constraints can be applied when calling a module to ensure stability.

**Example: Sourcing a Module from the Terraform Registry and Specifying a Version**

```hcl
module "example_module" {
  source  = "Azure/compute/azurerm"
  version = "1.1.1"
  # ... (pass any required variables here)
}
```

### Objective 4: Enhancing Configuration Reusability using Modules

Modules enhance reusability and modularity of your configurations. You can reuse modules across multiple environments and configurations, making your code DRY (Don’t Repeat Yourself).

**Example: Reusing a Module in Different Environments**

You might have different configurations for staging and production, and you can use the same module in both by passing different variables.

```hcl
module "example_module_staging" {
  source = "./example-module"
  environment = "staging"
}

module "example_module_production" {
  source = "./example-module"
  environment = "production"
}
```

### Challenges for Section 5

1. **Challenge 1**: Create a module in your "example" project containing configurations for creating a Virtual Network and Subnet.
2. **Challenge 2**: Utilize the module you created in a main configuration.

---

Embark on these challenges with the knowledge and examples provided. Through them, you will gain practical experience and mastery in creating, managing, and optimizing the use of Terraform Modules. Best of luck, and enjoy the process! 🌐🚀

---

Helpful URLs for these challenges:

- Terraform Modules
https://developer.hashicorp.com/terraform/language/modules

---
Work on completing the challenges listed above.  Try to complete them on your own without using the solution.  Once completed or if you get stuck continue on to the "Solution".

[Part 2: Solution](Part-02-Solution.md)
