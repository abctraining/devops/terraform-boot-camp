# Exercise #04 : Part 1
## Working with Outputs and Variables

---

### Welcome to Section 4 of the Terraform Boot Camp Workshop!

Welcome back, intrepid learners! In this section, we will delve into the world of Terraform variables, outputs, and the importance of managing sensitive data. Understanding how to parameterize your configurations and securely handle sensitive information is crucial in real-world Terraform projects. By the end of this section, you'll have a firm grasp of these fundamental concepts. Let's get started!

### Objective 1: Defining and Using Terraform Variables

Terraform variables allow you to parameterize your configurations, making them dynamic and reusable. Variables can be defined in your configuration and passed values during the execution. Here's a basic example:

```hcl
variable "vm_size" {
  description = "Size of the Virtual Machine"
  type        = string
  default     = "Standard_B1s"
}
```

You can then reference this variable in your resources:

```hcl
resource "azurerm_virtual_machine" "example" {
  # ...
  vm_size = var.vm_size
}
```

### Objective 2: Managing Terraform Outputs

Outputs allow you to expose specific values or information from your Terraform configuration. This is helpful for providing information to other parts of your infrastructure or for end-users. For example:

```hcl
output "vm_ip" {
  description = "Public IP Address of the Virtual Machine"
  value       = azurerm_virtual_machine.example.network_interface_ids[0].ip_configuration[0].public_ip_address
}
```

### Objective 3: Marking Sensitive Outputs

Some outputs may contain sensitive data, such as passwords or private keys. To handle this securely, you can mark an output as sensitive. For instance:

```hcl
output "vm_password" {
  description = "Admin Password for the Virtual Machine"
  value       = random_password.example.result
  sensitive   = true
}
```

### Objective 4: Using Outputs in Terraform Configurations

You can use outputs from one Terraform configuration as inputs in another. This interconnectivity is useful for creating complex infrastructures. Here's how you can reference an output from another configuration:

```hcl
resource "azurerm_virtual_machine" "example" {
  # ...
  admin_password = random_password.another_configuration.result
}
```

### Challenges for Section 4

1. **Challenge 1**: Define a variable for the VM size in your "example" project and use it in the VM resource configuration.
2. **Challenge 2**: Create an output for the public IP address of the VM in your "example" project.
3. **Challenge 3**: Mark the password output as sensitive.
4. **Challenge 4**: Put the contents of the sensitive value into a shell variable name $SECRET_PASSWORD.

---

Are you ready for these challenges? By the end of this section, you'll be well-versed in using variables, outputs, and managing sensitive information in your Terraform configurations. Don't hesitate to experiment with different variables and outputs to deepen your understanding. Good luck! 🚀

---

Helpful URLs for these challenges:

- Terraform AzureRM Provider
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs

- Terraform Random Provider
https://registry.terraform.io/providers/hashicorp/random/latest/docs

- Terraform Resource: random_password
https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password

- Terraform resource: azurerm_virtual_machine
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_machine

- Terraform State
https://developer.hashicorp.com/terraform/language/state

- Terraform Outputs
https://developer.hashicorp.com/terraform/language/values/outputs

- Terraform Variables
https://developer.hashicorp.com/terraform/language/values/variables

---
Work on completing the challenges listed above.  Try to complete them on your own without using the solution.  Once completed or if you get stuck continue on to the "Solution".

[Part 2: Solution](Part-02-Solution.md)
