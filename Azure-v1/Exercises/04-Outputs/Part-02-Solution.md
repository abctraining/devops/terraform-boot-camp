# Exercise #04 : Part 2
## Working with Outputs and Variables

---

### Section 4 Challenges Solutions

#### Challenge 1: Define a Variable for the VM Size

1. In your "labs" project directory, open or create a file named `variables.tf` and define a variable for the VM size:

```hcl
variable "vm_size" {
  description = "The size of the Virtual Machine."
  type        = string
  default     = "Standard_B1s"
}
```

2. In your `main.tf`, reference the variable in your VM resource configuration:

```hcl
resource "azurerm_virtual_machine" "my_vm" {
  # ...
  vm_size = var.vm_size
  # ...
}
```

3. It is already set as the default but the value for `var.vmsize` can be set explicitly In your `terraform.tfvars` file, giving a standard place to change it to something other then the default value:

```hcl
vm_size = "Standard_B1s"
```

#### Challenge 2: Create an Output for the Public IP Address of the VM

1. In your "labs" project directory, open or create a file named `outputs.tf`, then define an output for the public IP:

```hcl
output "vm_public_ip" {
  description = "The public IP address of the Virtual Machine."
  value       = azurerm_public_ip.example.ip_address
}
```

#### Challenge 3: Mark the Password Output as Sensitive

1. Modify the `outputs.tf` file, or where your password output is defined, and mark it as sensitive:

```hcl
output "vm_password" {
  description = "The randomly generated password for the VM."
  value       = random_password.example.result
  sensitive   = true
}
```
_Note: we are changing the name of the `myvm-password` output_

#### Challenge 4: Put the Contents of the Sensitive Value into a Shell Variable Named `$SECRET_PASSWORD`

After applying your Terraform configuration, you can set the sensitive output as a shell variable by running the following command in your terminal:

```bash
export SECRET_PASSWORD=$(terraform output -raw vm_password)
```

Note that this will make the `$SECRET_PASSWORD` variable available in your current shell session, allowing you to use it in further commands securely.

#### Applying the Configuration

1. In the "labs" directory, plan and apply the configuration:

```bash
terraform plan
terraform apply
```

2. Confirm the changes by typing `yes` when prompted.

---

You've successfully completed Section 4 challenges! You've learned to define and use variables, manage outputs, handle sensitive data, and utilize shell variables in conjunction with Terraform outputs. Keep honing your skills by experimenting with these features further. Well done! 🌟

---

This concludes part 2 of Hands-on Exercise #04.

Return to the [Exercise Page](../README.md)