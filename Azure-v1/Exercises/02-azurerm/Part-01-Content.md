# Exercise #02 : Part 1
## Managing Azure Resources using the azurerm Provider

---

### Welcome to Section 2 of the Terraform Boot Camp Workshop!

Welcome back to our exciting journey through the world of Terraform! In this section, we will delve deeper into managing Azure resources using the `azurerm` provider. We will explore how to define, configure, and manage Azure-specific resources, enhancing your capability to automate infrastructure on Azure seamlessly. Let’s dive in!

### Objective 1: Overview of the `azurerm` Terraform Provider

The `azurerm` provider is a plugin for Terraform that allows you to manage resources in Microsoft Azure. By configuring this provider, you can define resources like virtual machines, databases, networks, and many more within your Terraform configurations.

Here’s a basic example:

```hcl
provider "azurerm" {
  features {}
  skip_provider_registration = true
}
```

### Objective 2: Creating and Managing Azure Resource Groups

Resource groups are a way to manage and organize related Azure resources. Here’s how you can create a resource group:

```hcl
resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "East US"
}
```

This configuration creates a resource group named "example-resources" in the "East US" location.

### Objective 3: Managing Azure Virtual Networks and Subnets

Creating virtual networks and subnets is fundamental in managing network-related resources in Azure. Here’s how:

```hcl
resource "azurerm_virtual_network" "example" {
  name                = "example-network"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "example" {
  name                 = "example-subnet"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = ["10.0.1.0/24"]
}
```

### Objective 4: Connecting to Azure and Applying Configuration

After defining the resources, you apply the configuration to create them in Azure:

1. Initialize the configuration:

```bash
terraform init
```

2. Apply the configuration:

```bash
terraform apply
```

You'll be prompted to confirm that you want to create the resources. Type `yes` to proceed.

---

### Challenges for Section 2

1. **Challenge 1**: Configure the `azurerm` provider in your "example" project.
2. **Challenge 2**: Create an Azure Resource Group in a region of your choice.
3. **Challenge 3**: Define a virtual network and a subnet within the created resource group.
4. **Challenge 4**: Initialize and apply your configuration, creating the defined resources in Azure.

---

Ready to tackle these challenges? They are designed to strengthen your understanding and practical knowledge of managing Azure resources with Terraform. Good luck, and remember, practice makes perfect! 🚀

---

Helpful URLs for these challenges:

- Terraform AzureRM Provider
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs

- Terraform Random Provider
https://registry.terraform.io/providers/hashicorp/random/latest/docs

- Terraform resource: azurerm_resource_group
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group

- Terraform resource: azurerm_virtual_network
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/virtual_network

- Terraform resource: azurerm_subnet
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/subnet


---
Work on completing the challenges listed above.  Try to complete them on your own without using the solution.  Once completed or if you get stuck continue on to the "Solution".

[Part 2: Solution](Part-02-Solution.md)
