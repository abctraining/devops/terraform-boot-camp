# Exercise #02 : Part 2
## Managing Azure Resources using the azurerm Provider

---

### Section 2 Challenges Solutions

#### Challenge 1: Configure the `azurerm` provider in your "labs" project.

1. Open the `main.tf` file inside the "labs" project directory. 

2. Add the `azurerm` provider configuration at the top of the file:

```hcl
provider "azurerm" {
  features {}
  skip_provider_registration = true
}
```

3. Save the file and close the editor.

#### Challenge 2: Create an Azure Resource Group in a region of your choice.

1. Open the `main.tf` file in the "labs" project directory.

2. Add the following code to create an Azure Resource Group. Replace `"East US"` with your desired region:

```hcl
resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "East US"
}
```

3. Save the file.

#### Challenge 3: Define a virtual network and a subnet within the created resource group.

1. In the `main.tf` file, add the following code to create a virtual network and a subnet within the same resource group:

```hcl
resource "azurerm_virtual_network" "example" {
  name                = "example-network"
  resource_group_name = azurerm_resource_group.example.name
  location            = azurerm_resource_group.example.location
  address_space       = ["10.0.0.0/16"]
}

resource "azurerm_subnet" "example" {
  name                 = "example-subnet"
  resource_group_name  = azurerm_resource_group.example.name
  virtual_network_name = azurerm_virtual_network.example.name
  address_prefixes     = ["10.0.1.0/24"]
}
```

2. Save the file.

#### Challenge 4: Initialize and apply your configuration, creating the defined resources in Azure.

1. In the Cloud Shell, make sure you are in the "labs" project directory:

```bash
cd ~/clouddrive/labs
```

2. Initialize the Terraform project by running:

```bash
terraform init
```

3. Apply the configuration to create the defined resources in Azure:

```bash
terraform apply
```

You will be prompted to confirm the resource creation. Type `yes` and press Enter to proceed.

4. Once the operation completes, you will see the output with details of the created resources.

---

Congratulations! You have successfully completed the challenges for Section 2. You've now learned how to configure the `azurerm` provider, create an Azure Resource Group, define a virtual network and subnet, and apply the configuration to create these resources in Azure. Keep up the great work! 🌟

If you have any questions or need further assistance, feel free to ask.

---

This concludes part 2 of Hands-on Exercise #02.

Return to the [Exercise Page](../README.md)