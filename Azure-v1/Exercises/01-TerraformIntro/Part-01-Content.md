# Exercise #01 : Part 1
## Introduction to Terraform and Setting up the Environment

---

### Welcome to the Terraform Boot Camp Workshop!

Hello and welcome to the first section of our Terraform Boot Camp Workshop! In this section, we will dive into the foundational concepts of Terraform and set up our environment in the Azure Cloud Shell. We are thrilled to have you here, and we’re looking forward to exploring the fascinating world of Infrastructure as Code (IaC) together. Let’s get started!

### Objective 1: Introduction to Terraform: Overview and Key Concepts

Terraform is a powerful tool used for building, changing, and versioning infrastructure efficiently. It allows you to manage popular service providers and custom in-house solutions. Here are some key concepts to understand:

- **Configuration Files**: Written in HashiCorp Configuration Language (HCL), these files describe the infrastructure you want to deploy.
- **Providers**: These are plugins that Terraform uses to manage resources. Providers can be for cloud platforms like Azure, AWS, or services like GitHub.
- **Resources**: These are the infrastructure components like VMs, databases, or networks.

```hcl
provider "azurerm" {
  features {}
  skip_provider_registration = true
}

resource "azurerm_resource_group" "example" {
  name     = "example-resources"
  location = "East US"
}
```

### Objective 2: Setting Up the Cloud Shell Environment for Terraform

Azure Cloud Shell comes pre-installed with Terraform. To use Terraform in Cloud Shell:

1. Open Azure Cloud Shell.
2. Navigate to the `clouddrive` directory:

```bash
cd clouddrive
```

3. Create a directory for our project "example" and navigate into it:

```bash
mkdir example
cd example
```

### Objective 3: Using the `random` Terraform Provider to generate passwords

The `random` provider allows you to create random values, such as passwords. Below is an example of how to generate a random password:

```hcl
provider "random" {}

resource "random_password" "example" {
  length  = 16
  special = true
}
```

You can refer to the password in other parts of your configuration:

```hcl
output "generated_password" {
  value     = random_password.example.result
  sensitive = true
}
```

### Objective 4: Configuring and Initializing a Terraform Project

1. In the project directory, create a new file with a `.tf` extension, like `main.tf`, and add your configuration code.

2. Initialize the Terraform project. This command will download the necessary providers and set up the backend for state management.

```bash
terraform init
```

3. After initialization, you can apply the configuration using:

```bash
terraform apply
```

---

### Challenges for Section 1

1. **Challenge 1**: Setting up the Cloud Shell environment and navigating to the "clouddrive" folder.
2. **Challenge 2**: Writing a basic Terraform configuration file within the "example" project directory.
3. **Challenge 3**: Generating a random password using the `random` provider in your Terraform configuration.
4. **Challenge 4**: Initializing the Terraform project and exploring the initial state in the "example" project directory.

---

Good luck with the challenges! This is where the real learning begins—by doing. Remember, consistency and practice are key in mastering Terraform! 🚀


---

Helpful URLs for these challenges:

- Terraform AzureRM Provider
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs

- Terraform Random Provider
https://registry.terraform.io/providers/hashicorp/random/latest/docs

- Terraform State
https://developer.hashicorp.com/terraform/language/state

---
Work on completing the challenges listed above.  Try to complete them on your own without using the solution.  Once completed or if you get stuck continue on to the "Solution".

[Part 2: Solution](Part-02-Solution.md)
