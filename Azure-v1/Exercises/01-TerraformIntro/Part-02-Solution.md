# Exercise #01 : Part 2
## Introduction to Terraform and Setting up the Environment

---

### Section 1 Challenges Solutions

#### Challenge 1: Setting up the Cloud Shell environment and navigating to the "clouddrive" folder.

1. Open the Azure Portal and click on the "Cloud Shell" icon in the top-right corner.

2. In the Cloud Shell, ensure that you're in the home directory by running the following command:

```bash
cd ~
```

3. Next, navigate to the "clouddrive" folder using the following command:

```bash
cd clouddrive
```

#### Challenge 2: Writing a basic Terraform configuration file within the "labs" project directory.

1. Create a new directory for the project named "labs" and navigate into it:

```bash
mkdir labs
cd labs
```

2. Inside the "labs" directory, create a new Terraform configuration file named `main.tf` using the following command:

```bash
touch main.tf
```

3. Open the `main.tf` file in the editor of your choice and add the following basic configuration:

```hcl
provider "azurerm" {
  features {}
  skip_provider_registration = true
}
```

#### Challenge 3: Generating a random password using the `random` provider in your Terraform configuration.

1. Open the `main.tf` file inside the "labs" project directory.

2. Add the following code to generate a random password:

```hcl
provider "random" {}

resource "random_password" "generated_password" {
  length  = 16
  special = true
}
```

3. Save the file and close the editor.

#### Challenge 4: Initializing the Terraform project and exploring the initial state in the "labs" project directory.

1. In the Cloud Shell, ensure that you're in the "labs" directory:

```bash
cd ~/clouddrive/labs
```

2. Initialize the Terraform project by running the following command:

```bash
terraform init
```

3. After initialization, you can verify that the necessary providers are installed and the backend is set up.

```bash
terraform providers
```

This will display the list of providers used in your configuration.

4. You can validate your Terraform code with the following command:

```bash
terraform validate
```

This will let you know if there are any major errors in your HCL syntax.

6. You can see what the a Terraform run would plan to do with this command:

```bash
terraform plan
```

7. It is time to apply this Terraform code to the environment.
 
```bash
terraform apply
```

9. You can also check the initial state of your Terraform project using:

```bash
terraform state list
```

This will list the resources in your configuration and their current state.

---

You've successfully completed the challenges for Section 1! Great job! If you have any questions or need further assistance, feel free to ask. Keep up the good work! 🌟

---

This concludes part 2 of Hands-on Exercise #01.

Return to the [Exercise Page](../README.md)