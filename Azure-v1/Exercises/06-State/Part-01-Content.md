# Exercise #06 : Part 1
## State Management

---

### Welcome to Section 6 of the Terraform Boot Camp Workshop!

Welcome back, Cloud architects! 🌩️ In this vital section, we dive deep into the essence of Terraform's heart – the Terraform State. This hidden gem ensures the real-world resources' alignment with your configurations, acting as a bridge. Let’s master the manipulation, interpretation, and protection of the Terraform State together!

### Objective 1: Understanding the Importance of Terraform State

Terraform State is a crucial component that stores the current state of the infrastructure deployed. It holds metadata and properties of the managed resources, enabling Terraform to determine what actions are necessary to align the real-world infrastructure with the configuration.

**Example:**
When you execute `terraform apply`, Terraform uses the state file to identify the differences between your configuration and the actual infrastructure.

### Objective 2: Managing and Configuring Backend in Terraform

The backend in Terraform is responsible for storing the state file and providing an API for state locking and consistency checking.

**Example: Configuring a Backend**
```hcl
terraform {
  backend "azurerm" {
    resource_group_name   = "example-resources"
    storage_account_name  = "examplestoracc"
    container_name        = "tfstate"
    key                   = "terraform.tfstate"
  }
}
```

### Objective 3: Accessing and Interpreting State Data

You can inspect the contents of the state file to understand the current configuration of your managed resources.

**Example: Accessing State Data**
```bash
terraform show
```

### Objective 4: Protecting Sensitive Data in Terraform State

Sensitive data, such as passwords or keys, should be treated carefully. You can mark variables as sensitive, ensuring their values are not displayed in the console.

**Example: Marking a Variable as Sensitive**
```hcl
variable "db_password" {
  description = "Database password"
  type        = string
  sensitive   = true
}
```

### Challenges for Section 6

1. **Challenge 1**: Configure an Azure storage account backend in the "example" project for state management.
2. **Challenge 2**: Explore the state file using Terraform CLI commands and interpret the contained data.
3. **Challenge 3**: Make some changes to your configuration and observe how the state file is updated.
4. **Challenge 4**: Create a configuration containing sensitive data, such as passwords, and ensure this data is secured in the state file by marking it as sensitive.

---

Navigate through these challenges with an explorer’s curiosity. Unearth the hidden treasures of state management, unlocking new horizons in your Terraform journey! Good luck, and may the cloud be with you! 🌐🔐

---

Helpful URLs for these challenges:

- Terraform AzureRM Provider
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs

- Terraform Random Providor
https://registry.terraform.io/providers/hashicorp/random/latest/docs

- Terraform Resource: random_password
https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/password

- Terraform resource: azurerm_resource_group
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/resource_group

- Terraform resource: azurerm_storage_account
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_account

- Terraform resource: azurerm_storage_container
https://registry.terraform.io/providers/hashicorp/azurerm/latest/docs/resources/storage_container

- Terraform resource: random_integer
https://registry.terraform.io/providers/hashicorp/random/latest/docs/resources/integer

- Terraform Backend Configuration
https://developer.hashicorp.com/terraform/language/settings/backends/configuration

- Terraform State
https://developer.hashicorp.com/terraform/language/state

- Terraform Outputs
https://developer.hashicorp.com/terraform/language/values/outputs

- Terraform Variables
https://developer.hashicorp.com/terraform/language/values/variables


---

Helpful URLs for these challenges:

---
Work on completing the challenges listed above.  Try to complete them on your own without using the solution.  Once completed or if you get stuck continue on to the "Solution".

[Part 2: Solution](Part-02-Solution.md)
