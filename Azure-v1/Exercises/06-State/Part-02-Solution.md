# Exercise #06 : Part 2
## State Management

---

### Section 6 Challenges Solutions

#### Challenge 1: Configuring a Backend for State Management

First, let’s create an Azure storage account and container to store the Terraform state.

1. **Creating an Azure Storage Account and Container:**

In a new `state.tf`, add the following configurations:

```hcl
resource "azurerm_resource_group" "tfstate-rg" {
  name     = "stateManagementRG${random_integer.ri.result}"
  location = "East US"
}

resource "azurerm_storage_account" "tfstate-sa" {
  name                     = "statemanagementacc${random_integer.ri.result}"
  resource_group_name      = azurerm_resource_group.tfstate-rg.name
  location                 = azurerm_resource_group.tfstate-rg.location
  account_tier             = "Standard"
  account_replication_type = "LRS"
}

resource "random_integer" "ri" {
  min = 1000
  max = 9999
}

resource "azurerm_storage_container" "tfstate-sc" {
  name                  = "statecontainer${random_integer.ri.result}"
  storage_account_name  = azurerm_storage_account.tfstate-sa.name
  container_access_type = "private"
}

output "tfstate_resource_group" {
  value = azurerm_resource_group.tfstate-rg.name
}

output "tfstate_storage_account" {
  value = azurerm_storage_account.tfstate-sa.name
}

output "tfstate_storage_container" {
  value = azurerm_storage_container.tfstate-sc.name
}
```

2. **Create the storage container:**

Before continuing we need some vaules from the new resources.  We will need the values from the three tfstate outputs to configure the backend.

```shell
terraform validate
terraform plan
terraform apply
```

3. **Configuring the Backend:**

In a new `terraform.tf` file, configure the backend by referencing the created storage account and container:

```hcl
terraform {
  backend "azurerm" {
    resource_group_name   = ""
    storage_account_name  = ""
    container_name        = ""
    key                   = "terraform.tfstate"
  }
}
```

Note: Since the backend configuration doesn't accept computed values, you need to manually insert the actual names after the resources are created.

4. **Apply the changes to the Backend:**

Because you have modifide the terriform backend you will need to run `terraform init`.  Terraform will prompt you to approve the copy of the existing state into the new remote backend.

```shell
terraform init
```

#### Challenge 2: Examining and Interpreting the Terraform State File

1. Run the following command to inspect the current state:

```bash
terraform show
```

2. Review the output to understand the current state of your configuration and the details of your managed resources.

#### Challenge 3: Managing Changes to the State File

1. Make a change in your configuration, such as modifying the address space of a virtual network.

**`network.tf` (Modification)**

```hcl
module "network" {
  ...
  subnet_address_space = "10.42.33.0/24"
  ...
}
```

2. Apply the configuration to make the change.

```bash
terraform apply
```

3. Review the state file again using `terraform show` to see how it has been updated based on your changes.
#### Challenge 4: Ensuring Sensitive Data is Secure in the State File

1. Define sensitive data and mark it as sensitive in your configuration:

**`secret.tf`**

```hcl
variable "secret_password" {
  description = "A secret password"
  type        = string
  sensitive   = true
}

output "ssssshhh-secret" {
  value     = var.secret_password
  sensitive = true
}
```

2. Apply the configuration:

```bash
terraform apply
```

3. Because we have not set a value for the "secret password" terraform will prompt you to enter the value at the CLI.

4. Confirm that the sensitive data is not exposed in the command output after the `terrafrom apply`.

---

Congratulations on completing the challenges of Section 6! You've successfully managed and secured the Terraform state, enhancing the reliability and integrity of your configurations. Well done! 🚀🔐

---

This concludes part 2 of Hands-on Exercise #06.

Return to the [Exercise Page](../README.md)