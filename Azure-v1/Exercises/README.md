# Terraform Boot Camp - Azure - Labs

* [00 - Connect to Azure](00-Azure) - Azure - Cloud Shell`
* [01 - Intro to Terraform and the Environment](01-TerraformIntro)
* [02 - The azurerm Provider](02-azurerm)
* [03 - Deploying Virtual Machines](03-DeployVM)
* [04 - Working With Outputs](04-Outputs)
* [05 - Modules and Reusability](05-Modules)
* [06 - State Management](06-Stae)
* [07 - Conditional Expressions and Loops](07-Loops)
* [08 - Final Project and Review](08-FinalProject)



---
